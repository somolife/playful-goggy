# Creating a Single Page Application
[https://facebook.github.io/react/docs/installation.html]
```
npm install -g create-react-app
create-react-app hello-world
cd hello-world
npm start
```

## production build
>> npm run build will create an optimized build of your app in the build folder.
```
npm run build
npm install -g serve
serve -s
```

## now for real stuff!
npm i --save react-data-grid
npm i --save react-big-calendar
npm i --save bootstrap
npm i --save react-data-grid-addons

http://andrewhfarmer.com/react-ajax-best-practices/
