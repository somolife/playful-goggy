import React, { Component } from "react";
import PropTypes from 'prop-types';

import * as ep from '../api/endPoints';
import * as appState from '../constants/appState';
import { defaultValue } from '../constants/utils';


class Navigation extends Component {
    constructor(props) {
        super(props);
        const periodId = props.periodId;
        // const selected = props.selected; //todo: track selected to update selected/periodId just once!
        const nav = { next: false, postNext: false, prev: false, postPrev: false }
        this.state = { nav, periodId };

        this.handleGetNext = this.handleGetNext.bind(this);
        this.handleGetPrev = this.handleGetPrev.bind(this);
        this.handlePostNext = this.handlePostNext.bind(this);
        this.handlePostPrev = this.handlePostPrev.bind(this);
        this.handleGetToday = this.handleGetToday.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ periodId: nextProps.periodId });
    }

    handleGetNext() {
        const url = ep.nextUrlFn(this.state.periodId);
        this.getAnotherPeriod(url);
    }

    handleGetPrev() {
        const url = ep.prevUrlFn(this.state.periodId);
        this.getAnotherPeriod(url);
    }

    handleGetToday() {
        this.getAnotherPeriod(ep.constraintToday);
    }

    getAnotherPeriod(url) {
        ep.loadEndPoint(url,
            (data) => {
                const newPeriodId = defaultValue(data.id, 0);
                this.props.onUpdate({ periodId: newPeriodId });
            },
            (err) => { alert(`ERROR: ${JSON.stringify(err)}`) });
    }

    handlePostNext() {
        const url = ep.nextUrlFn(this.state.periodId);
        ep.postEndPoint(url, null, (res) => {
            this.setState({ selected: appState.specify });
            this.props.onUpdate({ selected: appState.specify });
            this.handleGetNext()
        });
    }

    handlePostPrev() {
        const url = ep.prevUrlFn(this.state.periodId);
        ep.postEndPoint(url, null, (res) => {
            this.setState({ selected: appState.specify });
            this.props.onUpdate({ selected: appState.specify });
            this.handleGetPrev()
        });
    }

    render() {
        return (
            <div className="container" style={{ paddingTop: '2em' }}>
                <div>
                    <button onClick={this.handlePostPrev} disabled={this.state.nav.postPrev}
                        className="btn btn-secondary" style={{ marginRight: '1em' }}>
                        create Previous
                    </button>
                    <button onClick={this.handleGetPrev} disabled={this.state.nav.prev}
                        className="btn btn-secondary" style={{ marginRight: '1em' }}>
                        prev
                    </button>
                    <button onClick={this.handleGetToday}
                        className="btn btn-secondary" style={{ marginRight: '1em' }}>
                        today
                </button>
                    <button onClick={this.handleGetNext} disabled={this.state.nav.post}
                        className="btn btn-secondary" style={{ marginRight: '1em' }}>
                        next
                    </button>
                    <button onClick={this.handlePostNext} disabled={this.state.nav.postNext}
                        className="btn btn-secondary" style={{ marginRight: '1em' }}>
                        create Next
                </button>
                </div>
            </div>
        );
    }
}

Navigation.propTypes = {
    onUpdate: PropTypes.func.isRequired,
    periodId: PropTypes.number.isRequired
};

export default Navigation;
