import React, { Component } from "react";
import PropTypes from 'prop-types';
import ScheduleStatus from './scheduleStatus';
import SimpleGrid from './simpleGrid';
import Navigation from './navigation';

class Schedule extends Component {
    constructor(props) {
        super(props);
        const selected = this.props.selected;
        const periodId = this.props.periodId;

        this.state = { selected, periodId };
    }

    componentWillReceiveProps(nextProps) {
        this.setState(nextProps);
    }

    render() {
        return (
            <div>
                <ScheduleStatus
                    periodId={this.state.periodId}
                    selected={this.state.selected} />
                <SimpleGrid
                    periodId={this.state.periodId}
                    selected={this.state.selected}
                    onUpdate={this.props.onUpdate} />
                <Navigation
                    periodId={this.state.periodId}
                    onUpdate={this.props.onUpdate} />
            </div>

        );
    }
}

Schedule.propTypes = {
    onUpdate: PropTypes.func.isRequired,
    selected: PropTypes.string.isRequired,
    periodId: PropTypes.number.isRequired
};


export default Schedule;
