import React, { Component } from "react";
import PropTypes from 'prop-types';

import * as ep from '../api/endPoints';
import * as state from '../constants/appState';

class ScheduleStatus extends Component {
    constructor(props) {
        super(props);
        const selected = this.props.selected;
        const periodId = props.periodId;
        this.state = { selected, periodId, conflicts: null, period: {}, shiftCover: [] };
    }

    componentWillMount() {
        this.loadUp(this.state.selected, this.state.periodId);
    }

    componentWillReceiveProps(nextProps) {
        this.loadUp(nextProps.selected, nextProps.periodId);
        this.setState({ selected: nextProps.selected, periodId: nextProps.periodId });
    }

    loadUp(selected, periodId) {
        // this.loadCover(periodId);
        this.loadPeriod(periodId);
        if (selected === state.review) {
            this.loadConflicts(periodId);
        }
    }

    loadCover(id) {
        ep.loadShiftCover(id, (data) => this.setState({ shiftCover: data.composite }));
    }

    loadConflicts(id) {
        ep.loadConflicts(id, (data) => this.setState({ conflicts: data }));
    }

    loadPeriod(id) {
        ep.loadPeriod(id, (data) => {
            this.setState({ period: data });
        });
    }

    renderShiftCover() {
        if (!this.state.shiftCover) return '';
        return this.state.shiftCover.map((element, i) => {
            return <div className='row' key={i}>
                <div className='col-md-1'>{JSON.stringify(Object.values(element.dimension.types))}</div>
                <div className='col-md-7'>{JSON.stringify(Object.values(element.requirement))}</div>
            </div>
        });
    }

    render() {
        const period = this.state.period;
        const pDefn = !!period ?
            <div>
                <h4>{period.startDate}</h4>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-md-2'>
                            {`${period.numEmployees} emp for ${period.shiftCoverPerDay} shifts over ${period.numDays} days`}
                        </div>
                        <div className='col-md-6'>
                            {!!this.state.conflicts ? JSON.stringify(this.state.conflicts) : ''}
                        </div>
                    </div>
                    {/* {this.renderShiftCover()} */}
                </div>
            </div>
            : '';

        return (
            <div className='container-fluid' style={{ paddingTop: '1em', textAlign: 'left' }}>
                {pDefn}
            </div>
        );
    }
}

ScheduleStatus.propTypes = {
    periodId: PropTypes.number.isRequired,
    selected: PropTypes.string.isRequired,
};

export default ScheduleStatus;
