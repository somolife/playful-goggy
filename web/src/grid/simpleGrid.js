import React, { Component } from "react";
import ReactDataGrid from 'react-data-grid';
import { RowRenderer, transformHeader, getRequests, getEmployees } from './scheduleRender';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import * as ep from '../api/endPoints';
import * as appState from '../constants/appState';

const { Toolbar, Data: { Selectors } } = require('react-data-grid-addons');

const getUrlFn = (selected) => selected === appState.specify ? ep.uiConstraints : ep.uiAssignments;


class SimpleGrid extends Component {
    constructor(props) {
        super(props);
        const selected = this.props.selected;
        const periodId = this.props.periodId;

        this.state = { selected, periodId, columns: [], rows: [], filters: {}};

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    getChildContext() { //to be used by CellRenderer
        return {
            empDayStatus: (id, key) => {
                if (!!this.state.rows[id] &&
                    !!this.state.rows[id][key]) {
                    return this.state.rows[id][key];
                }
                return false;
            }
        };
    }

    componentWillMount() {
        this.loadUp(this.state.periodId, this.state.selected);
    }

    componentWillReceiveProps(nextProps) {
        this.loadUp(nextProps.periodId, nextProps.selected);
    }

    loadUp(periodId, selected) {
        this.loadSchedule(selected, periodId);
    }

    loadSchedule(selected, periodId) {
        this.setState({ selected, periodId })
        const urlFn = getUrlFn(selected);
        ep.loadEndPoint(urlFn(periodId), (data) => {
            const comparer = (a, b) => {
                return (`${a['_name']}` > `${b['_name']}`) ? 1 : -1;
            };
            data.rows.sort(comparer);
            this.setState({
                columns: transformHeader(data.header, data.weekends),
                rows: data.rows,
                weekends: data.weekends,
            })
        });
    }

    getRows() {
        return Selectors.getRows(this.state);
    }

    getSize() {
        return this.getRows().length;
    }

    rowGetter() {
        return (rowIdx) => {
            let rows = this.getRows();
            return rows[rowIdx];
        };
    }

    handleFilterChange() {
        return (filter) => {
            let newFilters = Object.assign({}, this.state.filters);
            if (filter.filterTerm) {
                newFilters[filter.column.key] = filter;
            } else {
                delete newFilters[filter.column.key];
            }
            this.setState({ filters: newFilters });
        }
    }

    handleClearFilters() {
        return () => {
            this.setState({ filters: {} });
        }
    }

    handleGridRowsUpdated() {
        return ({ fromRow, toRow, updated }) => {
            let rows = this.state.rows.slice();
            for (let i = fromRow; i <= toRow; i++) {
                let rowToUpdate = rows[i];
                let updatedRow = update(rowToUpdate, { $merge: updated });
                rows[i] = updatedRow;
            }
            this.setState({ rows });
        }
    }

    handleGridSort() {
        return (sortColumn, sortDirection) => {
            const comparer = (a, b) => {
                if (sortDirection === 'ASC') {
                    return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
                } else if (sortDirection === 'DESC') {
                    return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
                }
            };

            const rows = sortDirection === 'NONE' ? this.state.originalRows.slice(0) : this.state.rows.sort(comparer);
            this.setState({ rows });
        };
    }

    handleSubmit() {
        ep.postEmployees(
            getEmployees(this.state.rows),
            res => console.log("postEmployee good!"));

        //yea, this triggers a schedule run; so leave it for last
        ep.postRequests(this.state.periodId,
            getRequests(this.state.rows),
            res => {
                console.log('postRequests good!')
            });
    }


    renderGrid() {
        return (
            <ReactDataGrid
                enableCellSelect={true}

                columns={this.state.columns}
                rowGetter={this.rowGetter()}
                rowsCount={this.getSize()}

                headerRowHeight={40}
                rowHeight={35}
                minHeight={520}

                toolbar={<Toolbar enableFilter={true} />}
                onAddFilter={this.handleFilterChange()}
                onClearFilters={this.handleClearFilters()}

                onGridRowsUpdated={this.handleGridRowsUpdated()}
                onGridSort={this.handleGridSort()}
                rowRenderer={RowRenderer}
            />
        );
    }

    renderPush() {
        return (
            <div style={{ paddingTop: '2em' }}>
                <button onClick={this.handleSubmit}
                    className="btn btn-primary" style={{ marginRight: '1em' }}
                >push changes</button>
            </div>
        );
    }

    render() {
        return (
            <div key={this.state.periodId}>
                {this.renderGrid()}
                {this.renderPush()}
            </div>

        );
    }
}

SimpleGrid.propTypes = {
    onUpdate: PropTypes.func.isRequired,
    selected: PropTypes.string.isRequired,
    periodId: PropTypes.number.isRequired
};

SimpleGrid.childContextTypes = {
    empDayStatus: React.PropTypes.func
};

export default SimpleGrid;

