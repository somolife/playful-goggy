import React, { Component } from "react";
import { Row, Cell } from 'react-data-grid';

const { Editors, Formatters } = require('react-data-grid-addons');
const { AutoComplete: AutoCompleteEditor, DropDownEditor } = Editors;

const headersToJson = {
    '_id': 'id',
    '_seniority': 'seniority',
    '_name': 'name',
    '_contract': 'contractCode',
    '_skill': 'skillCode',
}

//todo: figure out random colors based off string (shift assignment)
const cellStyling = {
    /* todo: temporary hack to showcase multiple locations */
    'nyc': 'working-cell-0',
    'jsy': 'working-cell-1',

    'HOS': 'working-cell-0',
    'MOR': 'working-cell-1',
    'CHA': 'working-cell-2',
    'SUC': 'working-cell-3',

    'W': 'working-cell',
    'RO': 'request-off-cell',
    'VA': 'request-vacay-cell',
    'RW': 'request-work-cell'
};

class CellRenderer extends Component {
    setScrollLeft(scrollBy) {
        // if you want freeze columns to work, you need to make sure you implement this as a pass through
        this.row.setScrollLeft(scrollBy);
    }

    render() {
        const status = this.context.empDayStatus(this.props.rowIdx, this.props.column.key);
        const className = cellStyling[status] || '';
        return <Cell ref={node => this.row = node} {...this.props} className={className} style={{ backgroundColor: '#123456' }} />;
    }
}

CellRenderer.contextTypes = {
    empDayStatus: React.PropTypes.func
};

export class RowRenderer extends React.Component {
    setScrollLeft(scrollBy) {
        // if you want freeze columns to work, you need to make sure you implement this as a pass through
        this.row.setScrollLeft(scrollBy);
    }

    render() {
        return (
            <div>
                <Row ref={node => this.row = node} {...this.props} cellRenderer={CellRenderer} />
            </div>
        );
    }
}

const dayOfWeekCellStyling = {
    'Sa': 'weekend-start-cell',
    'Su': 'weekend-end-cell'
};

const RequestEditor = <DropDownEditor options={['', 'RO', 'RW', 'VA']} />;
const ContractEditor = <DropDownEditor options={['DEF', 'PT', 'FT']} />; //todo: this should be filled from API

//todo: this needs heavy testing
export const transformHeader = (cols, weekends) => {
    const ret = cols.map(c => {
        const copy = Object.assign({}, c);

        copy.resizable = true;
        copy.filterable = true;

        //handle width
        if (copy.name === 'Name') {
            copy.width = 100;
        }
        else if (['ID', 'S', 'CTR', 'work'].includes(copy.name) > 0) {
            copy.width = 44;
        }
        else {
            copy.width = 80;
        }

        //handle editable
        if (['ID', 'work'].includes(copy.name) > 0) {
            copy.editable = false;
        } else {
            copy.editable = true;
        }

        const attributes = ['ID', 'Name', 'S', 'CTR', 'skill', 'work'];
        if (attributes.includes(copy.name)) {
            copy.locked = true;
            copy.sortable = true;
            if (copy.name === 'CTR') {
                copy.editor = ContractEditor;
            }
        } else {
            const style = dayOfWeekCellStyling[weekends[copy.key]] || '';
            copy.cellClass = style;
            copy.editor = RequestEditor
        }

        //console.log(`${copy.name}, ${JSON.stringify(copy)}`);

        return copy;
    });
    return ret;
};

const getRequestsFor = (name, rows) => {
    const selection = {};
    rows.forEach(r => {
        selection[parseInt(r._id, 10)] = Object.entries(r)
            .filter(e => e[1] === name)
            .map(e => e[0]);
    });
    return selection;
};

export const getRequests = (rows) => {
    //todo: workout diffs when posting instead of posting everything like a mad man
    const requests = { 'map': {} };
    requests['map']['DAY_OFF'] = getRequestsFor('RO', rows);
    requests['map']['DAY_ON'] = getRequestsFor('RW', rows);
    requests['map']['VACATION'] = getRequestsFor('VA', rows);
    return requests;
};

export const getEmployees = (rows) => {
    //todo: workout diffs when posting instead of posting everything like a mad man
    const emplHeaders = Object.keys(headersToJson);
    const selection = rows.map(r => {
        const asKvPairs = Object.entries(r).filter(e => emplHeaders.includes(e[0]));
        return asKvPairs.reduce((p, c) => { p[headersToJson[c[0]]] = c[1]; return p; }, {});
    });
    return selection;
};
