//I'm sure this is in a lib somewhere
export const isNullOrUndefined = (val) => typeof val === 'undefined' || val == null;

export const defaultValue = (val, defaultVal) => isNullOrUndefined(val) ? defaultVal : val;

export const log = (type) => console.log.bind(console, type);
