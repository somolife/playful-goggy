export const shiftCover = 'shiftCover';
export const start = 'start';
export const contracts = 'contracts';
export const employees = 'employees';
export const specify = 'specify';
export const review = 'review';

export const defaultState = start;
