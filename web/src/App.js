import React, { Component } from "react";

import * as state from "./constants/appState";

import ButtonGroup from './components/buttonGroup';
import StartForm from './components/startForm';
import ContractEditor from './components/contracts';
import ShiftCover from './components/shiftCover';
import EmployeeForm from './components/employeeForm';
import Schedule from "./grid/schedule";

import "./App.css";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = { selected: state.defaultState, periodId: 1 };
        this.appUpdate = this.appUpdate.bind(this);
    }

    getComponent() {
        switch (this.state.selected) {
            case state.start:
                return <div><StartForm periodId={this.state.periodId} onUpdate={this.appUpdate} /></div>;
            case state.contracts:
                return <div><ContractEditor onUpdate={this.appUpdate} /></div>;
            case state.employees:
                return <div><EmployeeForm /></div>;
            case state.shiftCover:
                return <div><ShiftCover /></div>;
            case state.specify:
                return <div><Schedule periodId={this.state.periodId} selected={state.specify} onUpdate={this.appUpdate} /></div>;
            case state.review:
                return <div><Schedule periodId={this.state.periodId} selected={state.review} onUpdate={this.appUpdate} /></div>;
            default:
                return null;
        }
    }

    appUpdate({ selected, periodId }) {
        this.setState(() => {
            selected = selected || this.state.selected;
            periodId = periodId === undefined ? this.state.periodId : periodId;
            return { selected, periodId }
        });
    }

    render() {
        return (
            <div className="App">
                <p className="App-intro">
                    roster+ <b>{this.state.selected}</b>
                </p>
                <div><ButtonGroup onUpdate={this.appUpdate} /></div>
                {this.getComponent()}
            </div>
        );
    }
}

export default App;
