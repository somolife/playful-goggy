import BigCalendar from "react-big-calendar";
import moment from "moment";

BigCalendar.momentLocalizer(moment);

export const MyCalendar = props => (
    <div>
        <BigCalendar
            events={myEventsList}
            startAccessor='startDate'
            endAccessor='endDate'
        />
    </div>
);
