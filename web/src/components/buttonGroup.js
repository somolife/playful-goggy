import React, { Component } from "react";
import PropTypes from 'prop-types';
import * as state from '../constants/appState';

class ButtonGroup extends Component {
    constructor(props) {
        super(props);
        this.state = { selected: 'start' };
    }

    handleClick(selected) {
        return () => {
            this.setState(() => {
                this.props.onUpdate({ selected });
                return { selected }
            })
        }
    }

    render() {
        return (
            <div className="">
                <button onClick={this.handleClick(state.start)}>start</button>
                <button onClick={this.handleClick(state.shiftCover)}>shiftCover</button>
                <button onClick={this.handleClick(state.employees)}>employees</button>
                <button onClick={this.handleClick(state.contracts)}>contracts</button>
                <button onClick={this.handleClick(state.specify)}>specify</button>
                <button onClick={this.handleClick(state.review)}>review</button>
                {/* <button disabled={true}>publish</button> */}
            </div>
        );
    }
}

ButtonGroup.propTypes = {
    onUpdate: PropTypes.func.isRequired,
};

export default ButtonGroup;