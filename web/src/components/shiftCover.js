import React, { Component } from 'react';
import Form from "react-jsonschema-form";

import * as ep from '../api/endPoints'
import { log } from '../constants/utils'
/* 
    {"requirement":{"defaultCover":7,
    "dayOfWeekCovers":[{"weekday":1,"cover":6},{"weekday":2,"cover":6}],
    "specificDateCovers":[]}
*/
const requirement = {
    type: "object", properties: {
        defaultCover: { type: "number", title: "Default cover" },
        dayOfWeekCovers: {
            type: "array", title: "Day of week cover",
            items: {
                type: "object",
                properties: {
                    weekday: { type: "number" }, cover: { type: "number" }
                }
            }
        },
        specificDateCovers: {
            type: "array", title: "Specific date cover",
            items: {
                type: "object",
                properties: {
                    dateStr: { type: "string", "format": "date" }, cover: { type: "number" }
                }
            }
        }
    }
}

/* "dimension":{"types":{"SKILL":"nurse","LOCATION":"nyc"}}} */
const dimension = {
    type: "object",
    properties: {
        types: {
            type: "object", properties: {
                SKILL: { type: "string" }, LOCATION: { type: "string" }
            }
        }
    }
}

const schemaForApi = {
    type: "array", title: "configure Shift Cover",
    items: {
        type: "object",
        properties: {
            dimension,
            requirement
        }
    }
}

const uiForRequirement = {
    dayOfWeekCovers: {
        classNames: 'row',
        "ui:options": {
            orderable: false
        }
    },
    specificDateCovers: {
        classNames: 'row',
        "ui:options": {
            orderable: false
        }
    },
};

const uiSchemaForApiData = {
    classNames: 'container-fluid',
    requirement: uiForRequirement
}

class ShiftCover extends Component {
    constructor(props) {
        super(props);
        //todo: setup props from App page
        this.state = { periodId: 1, shiftCovers: {} };
    }

    componentWillMount() {
        ep.loadShiftCover(this.state.periodId, (data) => this.setState({ shiftCovers: data }));
    }

    // componentWillReceiveProps(nextProps) {
    //     this.loadPeriod(nextProps.periodId);
    //     this.loadSchedule(nextProps.selected, nextProps.periodId);
    // }

    onSubmit() {
        return (event) => {
            ep.postShiftCover(this.state.periodId, { composite: event.formData }, res => console.log("postShiftCovers good!"));
        };
    }

    getForm() {
        return (
            <div>
                <Form schema={schemaForApi}
                    uiSchema={uiSchemaForApiData}
                    formData={this.state.shiftCovers.composite}
                    onChange={log("change")}
                    onSubmit={this.onSubmit()}
                    onError={log("errors")} />
            </div>
        );
    }

    render() {
        return (
            <div className="container" style={{ width: '35%', paddingTop: '4em' }}>
                {this.getForm()}
            </div >
        );
    }
}

export default ShiftCover;