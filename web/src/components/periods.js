import React from "react";
import { defaultValue } from '../constants/utils';
import * as state from '../constants/appState';

export const renderPeriods = (data, onPeriodSelect) => {

    if (data.length === 0) return '';

    return (
        <div className='container' style={{ paddingTop: '4em', width: '50%' }}>
            <table className='table'>
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">specify</th>
                        <th scope="col">review</th>
                        <th scope="col">num days</th>
                        <th scope="col">shift cover per day</th>
                        <th scope="col">num employees</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((row, i) =>
                        <tr key={i}>
                            <th key={`${i}-id`} scope="row">{defaultValue(row.id, 0)}</th>
                            <td key={`${i}-start`}>{
                                <button type="button"
                                    onClick={onPeriodSelect(state.specify, defaultValue(row.id, 0))}
                                    className="btn btn-secondary"
                                    style={{ marginRight: '1em' }}>
                                    {row.startDate}
                                </button>

                            }</td>
                            <td key={`${i}-review`}>{
                                <button type="button"
                                    onClick={onPeriodSelect(state.review, defaultValue(row.id, 0))}
                                    className="btn btn-primary"
                                    style={{ marginRight: '1em' }}>
                                    {row.startDate}
                                </button>

                            }</td>
                            <td key={`${i}-days`}>{row.numDays}</td>
                            <td key={`${i}-cover`}>{row.shiftCoverPerDay}</td>
                            <td key={`${i}-numFolks`}>{row.numEmployees}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
}
