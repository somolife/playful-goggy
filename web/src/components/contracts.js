import React, { Component } from 'react';
import * as ep from '../api/endPoints'

import Form from "react-jsonschema-form";

const schema = {
    type: "object",
    title: "Contract editor",
    required: ["code"],
    properties: {
        code: { type: "string", title: "Code", enum: ['DEF', 'PT', 'FT'] },
        weekendDefnCode: { type: "string", title: "Weekend Defn" },
        alternativeSkill: {
            type: "object",
            title: "Alt Skill",
            properties: {
                value: { type: "boolean", title: "Alt skill?", default: true },
                weight: { type: "number" }
            }
        },
        singleAssignmentPerDay: {
            type: "object",
            title: "Daily shift",
            properties: {
                value: { type: "boolean", title: "Single Assignment Per Day?", default: true },
                weight: { type: "number" }
            }
        },
        identicalShiftTypesDuringWeekend: {
            type: "object",
            title: "Weekend shift",
            properties: {
                value: { type: "boolean", title: "Weekend same as weekday?", default: true },
                weight: { type: "number" }
            }
        },
        totalAssignments: {
            type: "object",
            title: "Total assignments",
            properties: {
                min: {
                    type: "object",
                    title: "min",
                    properties: {
                        value: { type: "number" },
                        weight: { type: "number" }
                    }
                },
                max: {
                    type: "object",
                    title: "max",
                    properties: {
                        value: { type: "number" },
                        weight: { type: "number" }
                    }
                },
            }
        },
        consecutiveWorkingDays: {
            type: "object",
            title: "Consecutive working days",
            properties: {
                min: {
                    type: "object",
                    title: "min",
                    properties: {
                        value: { type: "number" },
                        weight: { type: "number" }
                    }
                },
                max: {
                    type: "object",
                    title: "max",
                    properties: {
                        value: { type: "number" },
                        weight: { type: "number" }
                    }
                },
            }
        },
        consecutiveFreeDays: {
            type: "object",
            title: "Consecutive free days",
            properties: {
                min: {
                    type: "object",
                    title: "min",
                    properties: {
                        value: { type: "number" },
                        weight: { type: "number" }
                    }
                },
                max: {
                    type: "object",
                    title: "max",
                    properties: {
                        value: { type: "number" },
                        weight: { type: "number" }
                    }
                },
            }
        },
        consecutiveWorkingWeekends: {
            type: "object",
            title: "Consecutive working wknds",
            properties: {
                min: {
                    type: "object",
                    title: "min",
                    properties: {
                        value: { type: "number" },
                        weight: { type: "number" }
                    }
                },
                max: {
                    type: "object",
                    title: "max",
                    properties: {
                        value: { type: "number" },
                        weight: { type: "number" }
                    }
                },
            }
        },
    },
};

const uiSchema = {
    code: {
        "ui:readonly": false,
        classNames: 'col-xs-6',
    },
    weekendDefnCode: {
        "ui:readonly": true,
        classNames: 'col-xs-6',
    },
    alternativeSkill: {
        classNames: 'form-group col-xs-4',
        value: { classNames: "col-xs-6" },
        weight: { classNames: "col-xs-6" }
    },
    singleAssignmentPerDay: {
        classNames: 'form-group col-xs-4',
        value: { classNames: "col-xs-6" },
        weight: { classNames: "col-xs-6" }
    },
    identicalShiftTypesDuringWeekend: {
        classNames: 'form-group col-xs-4',
        value: { classNames: "col-xs-6" },
        weight: { classNames: "col-xs-6" }
    },
    totalAssignments: {
        classNames: 'col-xs-6',
        min: {
            "ui:widget": "updown",
            classNames: "col-xs-6"
        },
        max: {
            "ui:widget": "updown",
            classNames: "col-xs-6"
        },
    },
    consecutiveWorkingDays: {
        classNames: 'col-xs-6',
        min: {
            "ui:widget": "updown",
            classNames: "col-xs-6"
        },
        max: {
            "ui:widget": "updown",
            classNames: "col-xs-6"
        },
    },
    consecutiveFreeDays: {
        classNames: 'col-xs-6',
        min: {
            "ui:widget": "updown",
            classNames: "col-xs-6"
        },
        max: {
            "ui:widget": "updown",
            classNames: "col-xs-6"
        },
    },
    consecutiveWorkingWeekends: {
        classNames: 'col-xs-6',
        min: {
            "ui:widget": "updown",
            classNames: "col-xs-6"
        },
        max: {
            "ui:widget": "updown",
            classNames: "col-xs-6"
        },
    }
}

const log = (type) => console.log.bind(console, type);

class ContractEditor extends Component {
    constructor(props) {
        super(props);
        this.state = { contracts: [{ code: 'DEF' }], selection: 'DEF' };
    }

    componentWillMount() {
        ep.loadContracts((data) => this.setState({ contracts: data }));
    }

    onChange() {
        return (event) => {
            if (event.formData.code !== this.state.selection) {
                this.setState({ selection: event.formData.code });
            }
        };
    }

    onSubmit() {
        return (event) => {
            //todo: check if updated values are there after posting
            ep.postContract(event.formData, res => console.log("postContract good!"));
        };
    }

    getForm() {
        return (
            <div>
                <Form schema={schema}
                    formData={this.state.contracts[this.state.selection]}
                    uiSchema={uiSchema}
                    onChange={this.onChange()}
                    onSubmit={this.onSubmit()}
                    onError={log("errors")} />
            </div>
        );
    }

    render() {
        return (
            <div className="container" style={{ width: '65%', paddingTop: '4em' }}>
                {this.getForm()}
            </div >
        );
    }
}

export default ContractEditor;