import React, { Component } from "react";
import Form from "react-jsonschema-form";

import * as ep from '../api/endPoints';
import { log } from '../constants/utils';


const getSchema = (data) => {
    return {
        type: "object", title: "Employee form",
        properties: {
            id: { type: "number", title: "ID" },
            name: { type: "string", title: "Name", enum: data.map(e => e.name) },
            contractCode: { type: "string", title: "Code" },
            seniority: { type: "number", title: "Seniority" },
            skillCode: { type: "string", title: "Skill" },
        }
    };
}


const uiSchema = {
    classNames: 'form-horizontal',
    id: {"ui:readonly": true}
}

class EmployeeForm extends Component {
    constructor(props) {
        super(props);
        this.state = { employees: [], selection: '' };
    }

    componentWillMount() {
        ep.loadEmployees((data) => {
            const employees = Object.values(data);
            this.setState({ employees, selection: employees[0].name })
        })
    }

    onChange() {
        return (event) => {
            if (event.formData.name !== this.state.selection) {
                this.setState({ selection: event.formData.name });
            }
        };
    }

    onSubmit() {
        return (event) => {
            ep.postEmployees([event.formData], res => console.log("postEmployees good!"));
        };
    }

    renderMainForm() {
        return (
            <div className="container" style={{ width: '30%', paddingTop: '4em' }}>
                <Form schema={getSchema(this.state.employees)}
                    formData={this.state.employees.find(e => e.name === this.state.selection)}
                    uiSchema={uiSchema}
                    onChange={this.onChange()}
                    onSubmit={this.onSubmit()}
                    onError={log("errors")} />
            </div>
        );
    }

    render() {
        return (
            <div>
                <p>{this.state.employees.length} employees!</p>
                {this.renderMainForm()}
            </div>
        )
    }
}

export default EmployeeForm;
