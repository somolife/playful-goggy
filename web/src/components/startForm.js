import React, { Component } from "react";
import PropTypes from "prop-types";
import Form from "react-jsonschema-form";

import * as ep from '../api/endPoints';
import * as state from '../constants/appState';
import { log } from '../constants/utils';
import { renderPeriods } from './periods';

const schema = {
    type: "object",
    title: "Let's build a schedule",
    required: ['numDays', 'startDate'],
    properties: {
        numDays: { type: "integer", title: "Number of days", default: 21 },
        startDate: {
            type: "string", title: "starting from ...",
            "format": "date"
        },
        shiftCoverPerDay: { type: "integer", title: "shift(s) to cover per day", default: 6 },
        numEmployees: { type: "integer", title: "number of employees", default: 10 },
    }
}

const uiSchema = {
    // classNames: 'col-xs-2',
    // numDays: {classNames: 'col-xs-2 '},
    // startDate: {classNames: 'col-xs-4 '},
    // shiftCoverPerDay: {classNames: 'col-xs-3 '},
    // numEmployees: {classNames: 'col-xs-3 '},
}

class StartForm extends Component {
    constructor(props) {
        super(props);
        this.state = { periods: [] };
        this.onPeriodSelect = this.onPeriodSelect.bind(this);
        this.nukeIt = this.nukeIt.bind(this);

    }

    componentWillMount() {
        ep.loadAllPeriods((data) => {
            this.setState({ periods: data })
        });
    }

    onSubmit() {
        return (event) => {
            const data = Object.assign({}, event.formData);
            ep.clearPeriods();
            ep.postPeriod(this.props.periodId, data,
                () => this.props.onUpdate({ selected: state.specify }));
        };
    }

    renderStartForm() {
        return (
            <div className="container" style={{ width: '20%', paddingTop: '4em' }}>
                <Form schema={schema}
                    // formData={this.state.contracts[this.state.selection]}
                    uiSchema={uiSchema}
                    onSubmit={this.onSubmit()}
                    onError={log("errors")} />
            </div>
        );
    }

    onPeriodSelect(selected = state.specify, periodId) {
        return () => {
            this.props.onUpdate({ selected, periodId })
        };
    }

    nukeIt() {
        ep.clearPeriods()
        this.setState({ periods: [] });
    }

    renderTheNuke() {
        if (this.state.periods.length === 0) return '';

        return (
            <div className='container' style={{ paddingTop: '3em' }}>
                <button type="button"
                    onClick={this.nukeIt}
                    className="btn btn-primary"
                    style={{ marginRight: '1em' }}>
                    __N_U_K_E___I_T_!!__
            </button>
            </div>
        );
    }

    render() {
        return (
            <div>
                {this.renderTheNuke()}
                {renderPeriods(this.state.periods, this.onPeriodSelect) || this.renderStartForm()}
            </div>
        );
    }
}

StartForm.propTypes = {
    periodId: PropTypes.number.isRequired,
    onUpdate: PropTypes.func.isRequired,
};

export default StartForm;