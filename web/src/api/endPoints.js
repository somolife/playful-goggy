import { postData, deleteData } from './utils';

const root = 'http://localhost:9000';
const consoleErr = error => console.error(JSON.stringify(error));

export const employees = `${root}/employees`;
export const contracts = `${root}/contracts`;
export const constraints = `${root}/constraints`;
export const assignments = `${root}/schedules`;
export const constraintToday = `${constraints}/today`;

export const forId = (path, id) => `${path}/${id}`;
export const constraintsFor = (id) => forId(constraints, id);
export const uiAssignments = (id) => forId(assignments, id);
export const uiConstraints = (id) => `${constraintsFor(id)}/schedule`;
export const requests = (id) => `${constraintsFor(id)}/requests`;
export const shiftCover = (id) => `${constraintsFor(id)}/dimensionCover`;
export const conflicts = (id) => `${uiAssignments(id)}/conflicts`;
export const nextUrlFn = (id) => `${forId(constraints, id)}/next`;
export const prevUrlFn = (id) => `${forId(constraints, id)}/prev`;


export const loadEndPoint = (url, onData, onError = consoleErr) => {
    console.log(`GET ${url}`);
    fetch(url, { cache: "force-cache" }) //todo: figure out the right caching
        .then(response => response.json())
        .then(data => onData(data))
        .catch(onError);
}

export const postEndPoint = (url, data, onRes, onError) => {
    console.log(`POST ${url}`);
    console.log(JSON.stringify(data));
    postData(url, data)
        .then(res => onRes(res))
        .catch(error => onError(error));
}

export const delEndPoint = (url, onError = consoleErr) => {
    console.log(`DEL ${url}`);
    deleteData(url)
        .catch(error => onError(error));
}


// handle Period

export const loadPeriod = (id, onData) => { loadEndPoint(constraintsFor(id), onData); }

export const postPeriod = (id, data, onRes) => {
    const url = forId(constraints, id);
    postEndPoint(url, data, res => onRes(res), consoleErr);
}

export const clearPeriods = () => {
    delEndPoint(constraints);
    delEndPoint(assignments);
}

export const loadAllPeriods = (onData) => { loadEndPoint(constraints, onData); }


export const loadConflicts = (id, onData) => {
    const url = conflicts(id);
    loadEndPoint(url, onData);
}


// handle ShiftCovers

export const loadShiftCover = (id, onData) => {
    const url = shiftCover(id);
    loadEndPoint(url, onData);
}

export const postShiftCover = (id, data, onRes) => {
    const url = shiftCover(id);
    postEndPoint(url, data, res => onRes(res), consoleErr);
}


// handle Requests

export const postRequests = (id, data, onRes) => {
    const url = requests(id);
    postEndPoint(url, data, res => onRes(res), consoleErr);
}


// handle Employees

export const loadEmployees = (onData) => { loadEndPoint(employees, onData) };

export const postEmployees = (data, onRes) => {
    data.forEach(e => {
        const url = forId(employees, e.id);
        postEndPoint(url, e, res => onRes(res), consoleErr);
    });
}

// handle Contracts

export const loadContracts = (onData) => { loadEndPoint(contracts, onData) }

export const postContract = (c, onRes) => {
    const url = forId(contracts, c.code);
    postEndPoint(url, c, res => onRes(res), consoleErr);
}
