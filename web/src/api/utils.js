
export function postData(url, data) {
    const dataStr = JSON.stringify(data);

    // Default options are marked with *
    return fetch(url, {
        body: dataStr, // must match 'Content-Type' header
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'omit', // include, *omit
        headers: { 'content-type': 'application/json' },
        method: 'POST', // *GET, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *same-origin
        redirect: 'follow', // *manual, error
        referrer: 'no-referrer', // *client
    })
}

export function deleteData(url) {
    // Default options are marked with *
    return fetch(url, {
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'omit', // include, *omit
        headers: { 'content-type': 'application/json' },
        method: 'DELETE', // *GET, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *same-origin
        redirect: 'follow', // *manual, error
        referrer: 'no-referrer', // *client
    })
}