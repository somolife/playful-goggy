
package org.optaplanner.examples.common.persistence;

import org.apache.commons.io.IOUtils;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.optaplanner.core.api.domain.solution.Solution;

import java.io.*;

/**
 * Created by nigeldev on 3/27/17.
 */
public abstract class AbstractJsonSolutionExporter extends AbstractSolutionExporter {

    protected static final String DEFAULT_OUTPUT_FILE_SUFFIX = "json";

    protected AbstractJsonSolutionExporter(SolutionDao solutionDao) {
        super(solutionDao);
    }

    protected AbstractJsonSolutionExporter(boolean withoutDao) {
        super(withoutDao);
    }

    public String getOutputFileSuffix() {
        return DEFAULT_OUTPUT_FILE_SUFFIX;
    }

    public abstract JsonOutputBuilder createJsonOutputBuilder();

    public void writeSolution(Solution solution, File outputFile) {
        OutputStream out = null;
        try {
            out = new FileOutputStream(outputFile);
            JsonOutputBuilder outputBuilder = createJsonOutputBuilder();
            outputBuilder.setBufferedWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(
                                    new FileOutputStream(outputFile), "UTF-8")));
            outputBuilder.setSolution(solution);
            outputBuilder.writeSolution();
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not write the file (" + outputFile.getName() + ").", e);
        } finally {
            IOUtils.closeQuietly(out);
        }
        logger.info("Exported: {}", outputFile);
    }

    public static abstract class JsonOutputBuilder extends OutputBuilder {

        protected BufferedWriter bufferedWriter;

        public void setBufferedWriter(BufferedWriter bufferedWriter) {
            this.bufferedWriter = bufferedWriter;
        }

        public abstract void setSolution(Solution solution);

        public abstract void writeSolution() throws IOException;

        // ************************************************************************
        // Helper methods
        // ************************************************************************

    }

}
