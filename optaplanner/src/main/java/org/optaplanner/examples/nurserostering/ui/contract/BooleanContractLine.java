package org.optaplanner.examples.nurserostering.ui.contract;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.optaplanner.examples.nurserostering.domain.contract.ContractLineType;

/**
 * Created by nigeldev on 3/29/17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BooleanContractLine extends ContractLine {

    private boolean enabled;
    private int weight;

    BooleanContractLine() {
    }

    @JsonCreator
    public BooleanContractLine(@JsonProperty("id") long id,
                               @JsonProperty("type") ContractLineType type,
                               @JsonProperty("opType") String opType,
                               @JsonProperty("enabled") boolean enabled,
                               @JsonProperty("weight") int weight) {
        super(id, type, opType);
        this.enabled = enabled;
        this.weight = weight;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public <T extends org.optaplanner.examples.nurserostering.domain.contract.ContractLine> T convert() {
        org.optaplanner.examples.nurserostering.domain.contract.BooleanContractLine line =
                new org.optaplanner.examples.nurserostering.domain.contract.BooleanContractLine();
        line.setId(this.getId());
        line.setContractLineType(this.getType());
        line.setEnabled(this.isEnabled());
        line.setWeight(this.getWeight());
        return (T) line;
    }
}
