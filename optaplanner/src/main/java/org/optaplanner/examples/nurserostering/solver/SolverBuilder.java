package org.optaplanner.examples.nurserostering.solver;

import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.constructionheuristic.ConstructionHeuristicPhaseConfig;
import org.optaplanner.core.config.constructionheuristic.ConstructionHeuristicType;
import org.optaplanner.core.config.solver.EnvironmentMode;
import org.optaplanner.core.config.solver.SolverConfig;
import org.optaplanner.core.config.solver.random.RandomType;
import org.optaplanner.core.config.solver.termination.TerminationConfig;
import org.optaplanner.examples.nurserostering.app.NurseRosteringApp;

import java.util.Arrays;

/**
 * @author nigeldev, @date 2/17/18
 */
public class SolverBuilder {

    SolverFactory<Solution> factory;
    SolverConfig config;

    public SolverBuilder() {
        this.factory = SolverFactory.createFromXmlResource(NurseRosteringApp.SOLVER_CONFIG);
        this.config = factory.getSolverConfig();
    }

    public SolverBuilder timeLimit(long timeLimit) {
        TerminationConfig terminationConfig = new TerminationConfig();
        terminationConfig.setSecondsSpentLimit(timeLimit);
        config.setTerminationConfig(terminationConfig);
        return this;
    }

    public SolverBuilder random(long seed) {
        config.setRandomSeed(seed);
        config.setRandomType(RandomType.JDK);
        return this;
    }

    public SolverBuilder envMode(EnvironmentMode mode) {
        config.setEnvironmentMode(mode);
        return this;
    }

    public SolverBuilder constructionHeuristic(ConstructionHeuristicType type) {
        ConstructionHeuristicPhaseConfig constructionHeuristicPhaseConfig = new ConstructionHeuristicPhaseConfig();
        constructionHeuristicPhaseConfig.setConstructionHeuristicType(type);
        config.setPhaseConfigList(Arrays.asList(constructionHeuristicPhaseConfig));
        return this;
    }

    public Solver<Solution> build() {
        return this.factory.buildSolver();
    }
}
