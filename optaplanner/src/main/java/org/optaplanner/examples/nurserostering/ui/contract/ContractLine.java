package org.optaplanner.examples.nurserostering.ui.contract;

import com.fasterxml.jackson.annotation.*;
import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.nurserostering.domain.contract.ContractLineType;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "opType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BooleanContractLine.class, name = "boolean"),
        @JsonSubTypes.Type(value = MinMaxContractLine.class, name = "minMax")
})
public abstract class ContractLine extends AbstractPersistable {

    private ContractLineType type;

    @JsonProperty("opType")
    private String opType;

    public abstract boolean isEnabled();

    public abstract <T extends org.optaplanner.examples.nurserostering.domain.contract.ContractLine> T convert();

    ContractLine() {
    }

    @JsonCreator
    public ContractLine(long id, ContractLineType type, @JsonProperty("opType") String opType) {
        super(id);
        this.type = type;
        this.opType = opType;
    }

    public ContractLineType getType() {
        return type;
    }

    public void setType(ContractLineType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type.toString();
    }

}
