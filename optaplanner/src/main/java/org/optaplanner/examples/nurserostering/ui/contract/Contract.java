package org.optaplanner.examples.nurserostering.ui.contract;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.nurserostering.domain.WeekendDefinition;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Contract extends AbstractPersistable {

    private String code;
    private String description;
    private WeekendDefinition weekendDefinition;
    private List<ContractLine> contractLines;

    private Contract() {
    }

    public Contract(long id, String code, String description, WeekendDefinition weekendDefinition, List<ContractLine> contractLines) {
        super(id);
        this.code = code;
        this.description = description;
        this.weekendDefinition = weekendDefinition;
        this.contractLines = contractLines;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public WeekendDefinition getWeekendDefinition() {
        return weekendDefinition;
    }

    public void setWeekendDefinition(WeekendDefinition weekendDefinition) {
        this.weekendDefinition = weekendDefinition;
    }

    public List<ContractLine> getContractLines() {
        return contractLines;
    }

    public void setContractLines(List<ContractLine> contractLines) {
        this.contractLines = contractLines;
    }

    @Override
    public String toString() {
        return code;
    }
}
