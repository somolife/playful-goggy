package org.optaplanner.examples.nurserostering.ui.contract;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.optaplanner.examples.nurserostering.domain.contract.ContractLineType;

/**
 * Created by nigeldev on 3/29/17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MinMaxContractLine extends ContractLine {
    ValueContractLine min;
    ValueContractLine max;

    MinMaxContractLine() {
    }

    @JsonCreator
    public MinMaxContractLine(@JsonProperty("id") long id,
                              @JsonProperty("type") ContractLineType type,
                              @JsonProperty("opType") String opType,
                              @JsonProperty("min") ValueContractLine min,
                              @JsonProperty("max") ValueContractLine max) {
        super(id, type, opType);
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean isEnabled() {
        return min.isEnabled() || max.isEnabled();
    }

    public ValueContractLine getMin() {
        return min;
    }

    public void setMin(ValueContractLine min) {
        this.min = min;
    }

    public ValueContractLine getMax() {
        return max;
    }

    public void setMax(ValueContractLine max) {
        this.max = max;
    }

    @Override
    public <T extends org.optaplanner.examples.nurserostering.domain.contract.ContractLine> T convert() {
        org.optaplanner.examples.nurserostering.domain.contract.MinMaxContractLine line =
                new org.optaplanner.examples.nurserostering.domain.contract.MinMaxContractLine();
        line.setId(this.getId());
        line.setContractLineType(this.getType());
        if(this.max == null){
            line.setMaximumEnabled(false);
        }else {
            line.setMaximumEnabled(this.max.enabled);
            line.setMaximumValue(this.max.value);
            line.setMaximumWeight(this.max.weight);
        }
        if(this.min == null){
            line.setMinimumEnabled(false);
        }else {
            line.setMinimumEnabled(this.min.enabled);
            line.setMinimumValue(this.min.value);
            line.setMinimumWeight(this.min.weight);

        }
        return (T) line;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class ValueContractLine {

        private boolean enabled;
        private int value;
        private int weight;

        public ValueContractLine() {
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }
    }
}
