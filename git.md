### tag it
```
git tag -a v1.4 -m "my version 1.4"

git show v1.4

git push origin v1.4
git push origin --tags      # all at once

git checkout v1.4
```

### delete that branch
```
git branch -d <branch_name>
git push origin --delete <branch_name>
```