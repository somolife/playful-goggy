## git commands

git stash

git checkout master

git fetch origin #gets you up to date w/ the origin

git merge origin/master

git clone --recursive git@github.com:sailthru/interviews

git push origin <branch>

git pull origin <branch>

###squash changes ...
git reset --soft HEAD~2 && gitcommit "rename threshold util and clean up"

### git config
> https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration

git config --global core.editor 
git config --global user.name "mdeonarine"
git config --global user.email mdeonarine@example.com


## Rename branch 
git branch -m old_branch new_branch         
### Delete the old branch    
git push origin :old_branch                 
### Push the new branch, set local branch to track the new remote
git push --set-upstream origin new_branch   


### delete branch locally and remotely
git branch -d branch_name
git push origin --delete <branchName>
