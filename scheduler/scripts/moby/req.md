# Moby

## total employees: 10

## contractual days
- FT [laura, angie, donia]
- PT [katie, sharon, khalida]
- 4d [naomi, roopam, francis]
- 3d [lynda]
- special requests
    - 1 day off every other week [khalida]
    - 1 day off per week: [katie, sharon]
    - always gets Tues off: [naomi]
- no wknds [roopam, francis, lynda]

## skills
- OB/GYN [laura, angie, donia, katie, sharon, khalida, roopam, francis, lynda] 
- GYN [naomi] 
- MD [laura, angie, donia, katie, sharon, khalida, naomi, roopam]
- NP [francis, lynda]


## locations
- hospital:
    - open: everyday
    - skills/cover: MD/1
    - pattern: 
        - 1 day on next day off
    - weeknds: set schedule; does wknd once every 3 wks;
    - include: [laura, angie, donia, katie, sharon, khalida]
- morristown:
    - open: M-F
    - skills/cover: 2-4, at least 1 MD
- succasunna:
    - open: M-F
    - skills/cover: 1-2, at least 1 MD
    - exclude: [naomi]
- chatham: 
    - open: M,W,F
    - skills/cover: MD/1
    - exclude: [katie, donia, naomi]
