# Moby feature analysis

## summary: current state
> provides roughly 80-90% of the schedule but requires improved conflict detection to figure what should be adjusted and manual intervention to correct

## currently supported
- number of employees
- contract: multiple contract types can specify day setup (e.g. 12,16,20 days) in 28 day period
- contract: special requests handled as a "RO"
- skill: multiple skills across the roster but just 1 skill per person. So e.g either MD or NP
- location/cover: multi location support for multiple contracts across days, also covering special days (e.g holidays)
- requests: dayoff and vacation requests supported
- requests: dayon requests supported

## feature missing
- conflict detection is currently too simplistic, enhance at the employee level. should be able to highlight multiple diff types of conflicts in the schedule
- contract: day setup (e.g. 3,4,5) per week
- contract: special requests should be associated w/ the employee and have its own code. E.g.
    - no wknds, always gets Tues off, etc
- location/cover: needs more combined skill requirement. e.g. MD+NP (for MOR, SUC), remember NPs can never be alone anywhere
- day patterns: e.g. on call today and off tomorrow; also special logic around the weekend
- skill: multiple per person to handle naomi? just GYN?

### Chris
- part toime vs full time functionality is a must
- laura is the only person who does call all weekend i.e. sat and sun together
- there is no morristown office on weekends
- seniority of partners is equal,  add a "seniority" list where angie, donia, sharon, khalida, laura, katie, naomi are partners
roopam, lynda, francis are not partners.  roopam is highest seniority among these three
- GYN skill = naomi never goes to hospital.  because hospital is for surgery and delivering babies  .  if you are gyn, you are not ob, so you dont delivery babies
- NP-s never go to hospital bc  they dont deliver babies OR do surgery
looks like katie goes to chatham, she shouldnt


