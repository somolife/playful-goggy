curl -H "Content-Type: application/json" http://localhost:9000/constraints/1 | jq . > period.1.json
curl -H "Content-Type: application/json" http://localhost:9000/contracts/PT | jq . > contract.PT.json
curl -H "Content-Type: application/json" http://localhost:9000/contracts/FT | jq . > contract.FT.json

for i in {1..10}
do
    curl -H "Content-Type: application/json" http://localhost:9000/employees/$i | jq . > employee.$i.json
done

curl -H "Content-Type: application/json" http://localhost:9000/constraints/1/requests | jq . > requests.1.json
curl -H "Content-Type: application/json" http://localhost:9000/constraints/1/dimensionCover | jq . > dimensionCover.json
