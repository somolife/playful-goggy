curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/contracts/PT -d @contract.PT.json
curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/contracts/FT -d @contract.FT.json

for i in {1..13}
do
    curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/employees/$i -d @employee.$i.json
done

curl -i -X DELETE -H "Content-Type: application/json" http://localhost:9000/constraints 
curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/constraints/1 -d @period.1.json

curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/constraints/1/dimensionCover -d @dimensionCover.json

curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/constraints/1/requests -d @requests.1.json
