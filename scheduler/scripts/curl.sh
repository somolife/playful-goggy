#!/bin/ksh
# POST
curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/constraints/1 -d @period.json
curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/employees/1000 -d @employee.json
curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/contracts/PT -d @contract.json
curl -i -X POST -H "Content-Type: application/json" http://localhost:9000/constraints/1/requests -d @requests.json

# GET
curl -i -X GET -H "Content-Type: application/json" http://localhost:9000/employees
curl -i -X GET -H "Content-Type: application/json" http://localhost:9000/contracts

curl -i -X GET -H "Content-Type: application/json" http://localhost:9000/constraints/1
curl -i -X GET -H "Content-Type: application/json" http://localhost:9000/constraints/1/requests
curl -i -X GET -H "Content-Type: application/json" http://localhost:9000/schedules/1



