package com.threeclicks.scheduler;

import com.codahale.metrics.health.HealthCheck;

public class RestAppHealth extends HealthCheck {
    private final String version;

    public RestAppHealth(String version) {
        this.version = version;
    }

    @Override
    protected Result check() throws Exception {
        return Result.healthy("OK with version: " + this.version);
    }
}