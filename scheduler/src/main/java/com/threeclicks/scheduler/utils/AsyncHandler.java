package com.threeclicks.scheduler.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * @author nigeldev, @date 2/28/18
 */
public class AsyncHandler implements Consumer<Callable> {
    private ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

    @Override
    public void accept(Callable callable) {
        singleThreadExecutor.submit(callable);
    }
}
