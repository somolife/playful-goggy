
package com.threeclicks.scheduler;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.threeclicks.scheduler.resources.ConstraintsResource;
import com.threeclicks.scheduler.resources.ContractsResource;
import com.threeclicks.scheduler.resources.EmployeesResource;
import com.threeclicks.scheduler.resources.SchedulesResource;
import io.dropwizard.Application;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.Arrays;
import java.util.EnumSet;

/**
 * @author nigeldev, @date 2/19/18
 */
public class RestApp extends Application<RestStubConfig> {

    public static void main(String[] args) throws Exception {
        new RestApp().run(args);
    }

    @Override
    public void run(RestStubConfig config, Environment env) {
        this.configureCors(env);

        Injector injector = Guice.createInjector(new RestModule());

        Arrays.asList(
                ContractsResource.class,
                EmployeesResource.class,
                SchedulesResource.class,
                ConstraintsResource.class)
                .forEach(clazz ->
                        env.jersey().register(injector.getInstance(clazz))
                );

        env.jersey().register(new JsonProcessingExceptionMapper(true));

        final RestAppHealth healthCheck = new RestAppHealth(config.getVersion());
        env.healthChecks().register("template", healthCheck);
        env.jersey().register(healthCheck);
    }

    private void configureCors(Environment environment) {
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

    }

}