package com.threeclicks.scheduler.mapper;

import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.Period;
import com.threeclicks.scheduler.mapper.rule.*;
import org.assertj.core.util.Lists;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * here's a thing to capture unsatisfied constraints
 *
 * @author nigeldev, @date 3/12/18
 */
public class ScheduleConflict {

    private List<Rule<NurseRoster, ?>> rosterChecks = Lists.newArrayList();

    public ScheduleConflict(Period p) {
        rosterChecks.add(new ShiftsPerPeriod());
        rosterChecks.add(new MinShiftPerEmployee());
        rosterChecks.add(new MaxShiftPerEmployee());
        rosterChecks.add(new SkillAssignmentPerEmployee());
        rosterChecks.add(new RequestsPerEmployee());
        rosterChecks.add(getPatternChecker());
    }

    /**
     * hardcoded for now, this should come off the {@link com.threeclicks.scheduler.api.Contract}
     * currently setup to work just for "moby"!
     */
    private PatternsPerEmployee getPatternChecker() {
        return new PatternsPerEmployee(CompositeKey.forLocation("HOS"));
    }

    public Map asBean(NurseRoster r) {
        return rosterChecks.stream()
                .collect(Collectors.toMap(
                        Rule::getName,
                        o -> o.apply(r)));
    }

}
