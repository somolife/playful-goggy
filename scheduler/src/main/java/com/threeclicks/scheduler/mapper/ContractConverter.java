package com.threeclicks.scheduler.mapper;


import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.threeclicks.scheduler.api.Period;
import org.optaplanner.examples.nurserostering.domain.WeekendDefinition;
import org.optaplanner.examples.nurserostering.domain.contract.Contract;
import org.optaplanner.examples.nurserostering.domain.contract.ContractLineType;
import org.optaplanner.examples.nurserostering.ui.contract.BooleanContractLine;
import org.optaplanner.examples.nurserostering.ui.contract.ContractLine;
import org.optaplanner.examples.nurserostering.ui.contract.MinMaxContractLine;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.threeclicks.scheduler.api.Contract.DEFAULT_BASIS;

/**
 * @author nigeldev, @date 2/19/18
 */
class ContractConverter {

    final Map<String, Contract> map = Maps.newHashMap();
    private Map<String, com.threeclicks.scheduler.api.Contract> apiMap;

    ContractConverter(Period p) {
        this.apiMap = p.contracts.stream().collect(Collectors.toMap(o -> o.code, o -> o));
        this.addContracts(p);
    }

    private void addContracts(Period p) {
        p.contracts.forEach(contract -> {
            Contract conv = this.convert(contract, p.numDays);
            map.putIfAbsent(conv.getCode(), conv);
        });
    }

    private Contract convert(com.threeclicks.scheduler.api.Contract contract, int basisInDays) {
        Preconditions.checkNotNull(contract);
//        Preconditions.checkArgument(totalAssignments > 0);
        Contract mapped = map.get(contract.code);
        if (mapped != null) {
            return mapped;
        }

        Contract conv = new Contract();
        conv.setCode(contract.code);

        long maxContractId = 0;
        long maxContractLineId = 0;

        conv.setId(maxContractId++);
//          conv.setDescription(contract.desc);
        conv.setWeekendDefinition(WeekendDefinition.valueOfCode(contract.weekendDefnCode));

        List<ContractLine> lines = Lists.newArrayList();

        lines.add(getBooleanContractLine(contract.singleAssignmentPerDay,
                ContractLineType.SINGLE_ASSIGNMENT_PER_DAY, maxContractLineId++));
        lines.add(getBooleanContractLine(contract.identicalShiftTypesDuringWeekend,
                ContractLineType.IDENTICAL_SHIFT_TYPES_DURING_WEEKEND, maxContractLineId++));
        lines.add(getBooleanContractLine(contract.alternativeSkill,
                ContractLineType.ALTERNATIVE_SKILL_CATEGORY, maxContractLineId++));

        lines.add(getMinMaxContractLine(this.prorate(contract.totalAssignments, basisInDays),
                ContractLineType.TOTAL_ASSIGNMENTS, maxContractId++));

        lines.add(getMinMaxContractLine(contract.consecutiveWorkingDays,
                ContractLineType.CONSECUTIVE_WORKING_DAYS, maxContractId++));
        lines.add(getMinMaxContractLine(contract.consecutiveFreeDays,
                ContractLineType.CONSECUTIVE_FREE_DAYS, maxContractId++));
        lines.add(getMinMaxContractLine(contract.consecutiveWorkingWeekends,
                ContractLineType.CONSECUTIVE_WORKING_WEEKENDS, maxContractId++));


        List<org.optaplanner.examples.nurserostering.domain.contract.ContractLine> convLines = lines.stream().map(contractLine -> {
            org.optaplanner.examples.nurserostering.domain.contract.ContractLine convert = contractLine.convert();
            convert.setContract(conv);
            return convert;
        }).collect(Collectors.toList());
        conv.setContractLineList(convLines);

        return conv;
    }

    private com.threeclicks.scheduler.api.Contract.MinMax prorate(
            com.threeclicks.scheduler.api.Contract.MinMax c,
            int dayBasis) {
        return new com.threeclicks.scheduler.api.Contract.MinMax(
                new com.threeclicks.scheduler.api.Contract.WeightedValue<>(
                        (c.min.value * dayBasis) / DEFAULT_BASIS, c.min.weight),
                new com.threeclicks.scheduler.api.Contract.WeightedValue<>(
                        (c.max.value * dayBasis) / DEFAULT_BASIS + 1, c.max.weight));
    }

    private MinMaxContractLine getMinMaxContractLine(
            com.threeclicks.scheduler.api.Contract.MinMax constraint,
            ContractLineType lineType,
            long contractId) {
        MinMaxContractLine.ValueContractLine min = new MinMaxContractLine.ValueContractLine();
        min.setEnabled(constraint.min.weight > 0);
        min.setValue(constraint.min.value);
        min.setWeight(constraint.min.weight);
        MinMaxContractLine.ValueContractLine max = new MinMaxContractLine.ValueContractLine();
        max.setEnabled(constraint.max.weight > 0);
        max.setValue(constraint.max.value);
        max.setWeight(constraint.max.weight);

        return new MinMaxContractLine(contractId, lineType, "minMax", min, max);
    }

    private BooleanContractLine getBooleanContractLine(
            com.threeclicks.scheduler.api.Contract.WeightedValue<Boolean> constraint,
            ContractLineType lineType,
            long contractId) {
        final BooleanContractLine line = new BooleanContractLine(contractId, lineType, "boolean",
                constraint.value, constraint.weight);
        line.setEnabled(constraint.weight > 0);
        return line;
    }

    public int getMaxWeight(String code) {
        return this.apiMap.get(code).maxWeight();
    }
}