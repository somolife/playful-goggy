package com.threeclicks.scheduler.mapper;
//todo: move this to the api package

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.Employee;
import org.optaplanner.examples.nurserostering.domain.DayOfWeek;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.domain.ShiftDate;
import org.optaplanner.examples.nurserostering.domain.request.DayOffRequest;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A "grid" format of the schedule. It may/may not include actual employee assignments
 * Captures [period, employee,request, assignments] data
 *
 * @author nigeldev, @date 2/18/18
 */
//todo: json ignore null fields
public class EmployeeSchedule {
    static String idKey = "_id";
    static String nameKey = "_name";
    static String seniorityKey = "_seniority";
    static String contractKey = "_contract";
    static String skillKey = "_skill";
    static String workKey = "_work";

    @JsonProperty
    List<Header> header = Lists.newArrayList();

    @JsonProperty
    List<Map<String, Object>> rows;

    @JsonProperty
    Map<String, String> weekends = Maps.newHashMap();

    private static final ImmutableMap<DayOffRequest.Type, String> requestTypeMapping = ImmutableMap.of(
            DayOffRequest.Type.REQUEST, "RO",
            DayOffRequest.Type.VACATION, "VA"
    );

    private static final Set<DayOfWeek> weekendDefnSet = Sets.newHashSet(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

    public static EmployeeSchedule from(NurseRoster roster, Map<Long, Employee> employeeMap) {
        EmployeeSchedule schedule = new EmployeeSchedule();
        addDateHeaders(roster, schedule);
        addWeekendDefn(roster, schedule);
        addEmployeeRows(mapEmployeeRows(roster, employeeMap), schedule);

        schedule.header.sort(Comparator.comparing(h -> h.key));
        addNamedHeaderToFront(schedule, "work", workKey);
        addNamedHeaderToFront(schedule, "skill", skillKey);
        addNamedHeaderToFront(schedule, "CTR", contractKey);
        addNamedHeaderToFront(schedule, "S", seniorityKey);
        addNamedHeaderToFront(schedule, "Name", nameKey);
        addNamedHeaderToFront(schedule, "ID", idKey);

        return schedule;
    }

    private static Map<Long, Row> mapEmployeeRows(NurseRoster roster, Map<Long, Employee> employeeMap) {
        Map<Long, Row> employeeRow = Maps.newHashMap();

        addEmployeeField(roster, employeeMap, employeeRow, idKey, Employee::getId);
        addEmployeeField(roster, employeeMap, employeeRow, nameKey, Employee::getName);
        addEmployeeField(roster, employeeMap, employeeRow, contractKey, Employee::getContractCode);
        addEmployeeField(roster, employeeMap, employeeRow, seniorityKey, Employee::getSeniority);
        addEmployeeField(roster, employeeMap, employeeRow, skillKey, Employee::getSkillCode);

        addWorkingAssignments(roster, employeeRow);
        addDayOffRequests(roster, employeeRow);
        addDayOnRequests(roster, employeeRow);

//        List<String> dates = schedule.header.stream().map(h -> h.key).collect(Collectors.toList());
//        schedule.rows.forEach(m -> {
//            dates.stream().filter(s -> !m.keySet().contains(s)).forEach(s -> m.putIfAbsent(s, "FO"));
//        });

        return employeeRow;
    }

    private static void addEmployeeRows(Map<Long, Row> employeeRow, EmployeeSchedule schedule) {
        schedule.rows = employeeRow.values().stream()
                .map(row -> row.data).collect(Collectors.toList());
    }

    private static void addNamedHeaderToFront(EmployeeSchedule schedule, String name2, String nameKey) {
        schedule.header.add(0, new Header(name2, nameKey));
    }

    private static void addDayOffRequests(NurseRoster roster, Map<Long, Row> employeeRow) {
        roster.getEmployeeList().stream()
                .filter(employee -> employee.getDayOffRequestMap() != null)
                .forEach(employee -> {
                    Row row = getOrDefaultRow(employeeRow, employee.getId());
                    employee.getDayOffRequestMap().forEach((shiftDate, dayOffRequest) -> {
                        row.data.putIfAbsent(shiftDate.getDateString(),
                                requestTypeMapping.get(dayOffRequest.getType()));
                    });
                });
    }

    private static void addDayOnRequests(NurseRoster roster, Map<Long, Row> employeeRow) {
        roster.getEmployeeList().stream()
                .filter(employee -> employee.getDayOnRequestMap() != null)
                .forEach(employee -> {
                    Row row = getOrDefaultRow(employeeRow, employee.getId());
                    employee.getDayOnRequestMap()
                            .forEach((shiftDate, request) ->
                                    row.data.putIfAbsent(shiftDate.getDateString(), "RW"));
                });
    }

    private static void addEmployeeField(NurseRoster roster,
                                         Map<Long, Employee> employeeMap,
                                         Map<Long, Row> employeeRow,
                                         String key, Function<Employee, Object> fn) {
        roster.getEmployeeList().forEach(employee ->
                getOrDefaultRow(employeeRow, employee.getId())
                        .data.putIfAbsent(key, fn.apply(employeeMap.get(employee.getId()))));
    }

    private static void addWorkingAssignments(NurseRoster roster, Map<Long, Row> employeeRow) {
        roster.getShiftAssignmentList().stream()
                .filter(shiftAssignment -> shiftAssignment.getEmployee() != null)
                .forEach(assignment -> {
                    final Row row = getOrDefaultRow(employeeRow, assignment.getEmployee().getId());
                    final Integer count = (Integer) row.data.computeIfAbsent(workKey, s -> 0);
                    row.data.put(workKey, count + 1);
                    row.data.put(
                            assignment.getShiftDate().getDateString(),
                            getLocation(assignment.getShiftType().getCode()));
                });
    }

    private static String getLocation(String code) {
        final String s = CompositeKey.valueFromJson(CompositeKey.Type.LOCATION, code);
        return s == null ? "W" : s;
    }

    private static Row getOrDefaultRow(Map<Long, Row> employeeRow, Long id) {
        return employeeRow.computeIfAbsent(id, aLong -> new Row());
    }

    private static void addDateHeaders(NurseRoster roster, EmployeeSchedule schedule) {
        roster.getShiftDateList().stream()
                .map(ShiftDate::getDateString)
                .collect(Collectors.toSet())
                .forEach(s -> schedule.header.add(new Header(shortDate(s), s)));
    }

    private static void addWeekendDefn(NurseRoster roster, EmployeeSchedule schedule) {
        roster.getShiftDateList().stream()
                .filter(EmployeeSchedule::isWeekend)
                .forEach(shiftDate -> schedule.weekends.put(
                        shiftDate.getDateString(),
                        shiftDate.getDayOfWeek().getLabel()));
    }


    private static boolean isWeekend(ShiftDate shiftDate) {
        return weekendDefnSet.contains(shiftDate.getDayOfWeek());
    }

    public static String shortDate(String longDate) {
        String[] split = longDate.split("-");
        return String.format("%s/%s", split[1], split[2]);
    }

    static class Header {
        @JsonProperty
        String name;
        @JsonProperty
        String key;

        @JsonCreator
        Header(@JsonProperty("name") String name,
               @JsonProperty("key") String key) {
            this.name = name;
            this.key = key;
        }
    }

    private static class Row {
        Map<String, Object> data = Maps.newHashMap();
    }
}
