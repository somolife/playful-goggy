package com.threeclicks.scheduler.mapper.rule;

import org.optaplanner.examples.nurserostering.domain.NurseRoster;

import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.*;

/**
 * @author nigeldev, @date 3/30/18
 */
public class RequestsPerEmployee extends Rule<NurseRoster, Map> {
    public RequestsPerEmployee() {
        super(RequestsPerEmployee.class.getSimpleName());
    }

    @Override
    public Map apply(NurseRoster nurseRoster) {
        final Map<Long, Set<Integer>> assignmentsByDate = this.getAssignments(nurseRoster)
                .collect(groupingBy(r -> r.getEmployee().getId(), mapping(r -> r.getShiftDate().getDayIndex(), toSet())));
        return nurseRoster.getDayOffRequestList().stream()
                .filter(r -> assignmentsByDate.get(r.getEmployee().getId()).contains(r.getShiftDate().getDayIndex()))
                .collect(groupingBy(r -> r.getEmployee().getId(), mapping(r -> r.getShiftDate().getDayIndex(), toSet())));
    }

}
