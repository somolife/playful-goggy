package com.threeclicks.scheduler.mapper.rule;

import com.google.common.collect.ImmutableMap;
import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.pattern.CoverDimensionFollowedByFreeDay;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.domain.ShiftAssignment;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

/**
 * basic case: any MD scheduled to work the HOS shift, must get the next day off
 *
 * @author nigeldev, @date 3/26/18
 */
public class PatternsPerEmployee extends Rule<NurseRoster, Map> {

    private CoverDimensionFollowedByFreeDay coverDimensionFollowedByFreeDay;

    public PatternsPerEmployee(CompositeKey compositeKey) {
        super(PatternsPerEmployee.class.getSimpleName());
        this.coverDimensionFollowedByFreeDay = new CoverDimensionFollowedByFreeDay(compositeKey);
    }

    @Override
    public Map apply(NurseRoster nurseRoster) {
        final CompositeKey dimension = coverDimensionFollowedByFreeDay.getCompositeKey();

        final List<ShiftAssignment> list = this.getAssignments(nurseRoster).collect(Collectors.toList());

        final Map<Long, Set<Integer>> matchedDays = list.stream()
                .filter(a -> dimension.partialMatch(a.getShiftType().getCode()))
                .collect(groupingBy(a -> a.getEmployee().getId(), mapping(a -> a.getShiftDate().getDayIndex(), toSet())));

        final Map<Long, Set<Integer>> allDays = list.stream()
                .filter(assignment -> matchedDays.keySet().contains(assignment.getEmployee().getId()))
                .collect(groupingBy(a -> a.getEmployee().getId(), mapping(a -> a.getShiftDate().getDayIndex(), toSet())));

        final Map<Long, Set<Integer>> res = matchedDays.entrySet().stream()
                .map(e -> new AbstractMap.SimpleEntry<>(e.getKey(), e.getValue().stream()
                        .filter(i -> allDays.get(e.getKey()).contains(i + 1))
                        .map(i -> i + 1)
                        .collect(toSet())))
                .filter(e -> !e.getValue().isEmpty())
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

        return ImmutableMap.of("coverDimensionFollowedByFreeDay", ImmutableMap.of(dimension.toJson(), res));
    }
}
