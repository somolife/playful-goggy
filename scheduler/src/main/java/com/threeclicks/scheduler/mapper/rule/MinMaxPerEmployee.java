package com.threeclicks.scheduler.mapper.rule;

import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.domain.contract.ContractLine;
import org.optaplanner.examples.nurserostering.domain.contract.ContractLineType;
import org.optaplanner.examples.nurserostering.domain.contract.MinMaxContractLine;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author nigeldev, @date 3/22/18
 */
abstract class MinMaxPerEmployee extends Rule<NurseRoster, List> {

    MinMaxPerEmployee(String name) {
        super(name);
    }

    @Override
    public List apply(NurseRoster nurseRoster) {
        return metEmployeeMinShiftRequirement(nurseRoster);
    }

    private List metEmployeeMinShiftRequirement(NurseRoster roster) {
        final Map<Long, Long> employeeAssignmentMap = getEmployeeAssignmentMap(roster);
        final Predicate<ContractLine> totalAssignments = getContractLinePredicate();
        final Map<Long, MinMaxContractLine> lineMap = roster.getEmployeeList().stream().
                collect(Collectors.toMap(o -> o.getId(), o -> o.getContract().getContractLineList().stream()
                        .filter(ContractLine::isEnabled)
                        .filter(totalAssignments)
                        .map(contractLine -> (MinMaxContractLine) contractLine).findFirst().get()));

        return getList(employeeAssignmentMap, lineMap);
    }

    abstract List getList(Map<Long, Long> employeeAssignmentMap, Map<Long, MinMaxContractLine> lineMap);

    private Predicate<ContractLine> getContractLinePredicate() {
        return contractLine -> contractLine.getContractLineType().equals(ContractLineType.TOTAL_ASSIGNMENTS);
    }

    private Map<Long, Long> getEmployeeAssignmentMap(NurseRoster roster) {
        return this.getAssignments(roster)
                .collect(Collectors.groupingBy(o -> o.getEmployee().getId(), Collectors.counting()));
    }
}
