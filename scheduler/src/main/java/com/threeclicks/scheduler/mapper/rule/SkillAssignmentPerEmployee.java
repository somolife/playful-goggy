package com.threeclicks.scheduler.mapper.rule;

import com.threeclicks.scheduler.api.CompositeKey;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;

import java.util.Map;
import java.util.Set;

import static com.threeclicks.scheduler.api.CompositeKey.valueFromJson;
import static java.util.stream.Collectors.*;

/**
 * some roles may have constraints on the day they are required to work or get off
 * case1: PT employee is not supposed to work weekends but was assigned a Sat shift
 * case2: PT employee should not be assigned to a HOS shift
 *
 * @author nigeldev, @date 3/26/18
 */
public class SkillAssignmentPerEmployee extends Rule<NurseRoster, Map> {

    public SkillAssignmentPerEmployee() {
        super(SkillAssignmentPerEmployee.class.getSimpleName());
    }

    @Override
    public Map apply(NurseRoster nurseRoster) {
        return employeeSkillsAssigned(nurseRoster, getEmployeeSkills(nurseRoster));
    }

    private Map<Long, Set<Integer>> employeeSkillsAssigned(NurseRoster nurseRoster, Map<Long, Set<String>> skillMap) {
        return this.getAssignments(nurseRoster)
                .filter(assignment -> {
                    final String skill = valueFromJson(CompositeKey.Type.SKILL, assignment.getShiftType().getCode());
                    final Long id = assignment.getEmployee().getId();
                    return !skillMap.get(id).contains(skill);
                })
                .collect(groupingBy(s -> s.getEmployee().getId(), mapping(s -> s.getShiftDate().getDayIndex(), toSet())));
    }

    private Map<Long, Set<String>> getEmployeeSkills(NurseRoster nurseRoster) {
        return nurseRoster.getSkillProficiencyList().stream()
                .collect(groupingBy(s -> s.getEmployee().getId(), mapping(s -> s.getSkill().getCode(), toSet())));
    }
}
