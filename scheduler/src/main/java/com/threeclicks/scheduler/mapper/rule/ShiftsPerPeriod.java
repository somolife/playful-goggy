package com.threeclicks.scheduler.mapper.rule;

import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.domain.Shift;
import org.optaplanner.examples.nurserostering.domain.ShiftAssignment;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

/**
 * @author nigeldev, @date 3/22/18
 */
public class ShiftsPerPeriod extends Rule<NurseRoster, Map> {

    public ShiftsPerPeriod() {
        super(ShiftsPerPeriod.class.getSimpleName());
    }

    @Override
    public Map apply(NurseRoster nurseRoster) {
        return totalAssignedByDay(nurseRoster, totalShiftsByDay(nurseRoster));
    }

    private Map<Integer, Integer> totalShiftsByDay(NurseRoster roster) {
        return roster.getShiftList().stream()
                .collect(groupingBy(
                        s -> s.getShiftDate().getDayIndex(),
                        summingInt(Shift::getRequiredEmployeeSize)));
    }

    private Map<Integer, Integer> totalAssignedByDay(NurseRoster roster, Map<Integer, Integer> required) {
        final Map<Integer, Integer> collect = getAssignments(roster)
                .collect(groupingBy(a -> a.getShiftDate().getDayIndex(), summingInt(s -> 1)));
        return required.entrySet().stream()
                .filter(e -> !e.getValue().equals(collect.get(e.getKey())))
                .collect(toMap(Map.Entry::getKey, e -> e.getValue() - collect.getOrDefault(e.getKey(), 0)));
    }

}
