package com.threeclicks.scheduler.mapper;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.threeclicks.scheduler.api.Period;
import org.assertj.core.util.Sets;
import org.optaplanner.examples.nurserostering.domain.Employee;
import org.optaplanner.examples.nurserostering.domain.Skill;
import org.optaplanner.examples.nurserostering.domain.SkillProficiency;
import org.optaplanner.examples.nurserostering.domain.contract.Contract;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author nigeldev, @date 3/12/18
 */
public class EmployeeConverter {
    final Map<Long, Employee> map = Maps.newHashMap();
    final Map<Long, com.threeclicks.scheduler.api.Employee> apiMap = Maps.newHashMap();
    final List<Employee> list = Lists.newArrayList();
    final Set<Skill> skills = Sets.newHashSet();
    final List<SkillProficiency> proficiencyList = Lists.newArrayList();

    final private ContractConverter contracts;

    private final AtomicLong skillId = new AtomicLong(0);
    private final AtomicLong proficiencyId = new AtomicLong(0);

    EmployeeConverter(Period p, ContractConverter contracts) {
        this.contracts = contracts;
        this.addEmployees(p);
    }

    private void addEmployees(Period p) {
        for (com.threeclicks.scheduler.api.Employee employee : p.employees) {
            Employee converted = getEmployee(employee);
            converted.setContract(getContract(employee.getContractCode()));

            apiMap.put(employee.getId(), employee);
            map.put(employee.getId(), converted);
            list.add(converted);

            final Skill skill = getSkill(employee);
            skills.add(skill);
            proficiencyList.add(getSkillProficiency(converted, skill));
        }
    }

    private SkillProficiency getSkillProficiency(Employee converted, Skill skill) {
        final SkillProficiency proficiency = new SkillProficiency();
        proficiency.setEmployee(converted);
        proficiency.setSkill(skill);
        proficiency.setId(proficiencyId.incrementAndGet());
        return proficiency;
    }

    private Skill getSkill(com.threeclicks.scheduler.api.Employee employee) {
        final Skill skill = new Skill();
        skill.setId(skillId.incrementAndGet());
        skill.setCode(employee.getSkillCode());
        return skill;
    }

    public static Employee getEmployee(com.threeclicks.scheduler.api.Employee employee) {
        Employee e = new Employee();
        e.setId(employee.getId());
        e.setName(employee.getName());
        e.setCode(employee.getName());
        e.setDayOffRequestMap(Maps.newHashMap());
        return e;
    }

    private Contract getContract(String code) {
        return contracts.map.get(code);
    }
}
