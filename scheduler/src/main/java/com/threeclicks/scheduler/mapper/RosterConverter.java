package com.threeclicks.scheduler.mapper;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.threeclicks.scheduler.api.Period;
import com.threeclicks.scheduler.api.Request;
import org.optaplanner.examples.nurserostering.domain.*;
import org.optaplanner.examples.nurserostering.domain.request.DayOffRequest;
import org.optaplanner.examples.nurserostering.domain.request.DayOnRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author nigeldev, @date 2/16/18 9:50 AM
 */
public class RosterConverter {

    private static final ImmutableMap<Request.Type, DayOffRequest.Type> dayOffTypeMapping = ImmutableMap.of(
            Request.Type.DAY_OFF, DayOffRequest.Type.REQUEST,
            Request.Type.VACATION, DayOffRequest.Type.VACATION
    );
    private ContractConverter contracts;
    private EmployeeConverter employees;
    private ShiftDateConverter shiftDates;

    public NurseRoster convert(Period p) {
        NurseRoster roster = createRoster();

        this.shiftDates = new ShiftDateConverter(p);
        roster.setShiftDateList(this.shiftDates.dates);
        addNurseRosterInfo(roster);

        this.contracts = new ContractConverter(p);
        addContracts(roster);

        this.employees = new EmployeeConverter(p, this.contracts);
        roster.setEmployeeList(this.employees.list);
        roster.setSkillList(Lists.newArrayList(this.employees.skills));
        roster.setSkillProficiencyList(this.employees.proficiencyList);

        ShiftConverter shiftConverter = new ShiftConverter(p.getShiftCover(),
                this.shiftDates.dates, this.employees.skills);
        roster.setShiftTypeList(shiftConverter.shiftTypes);
        roster.setShiftList(shiftConverter.shifts);
        roster.setShiftTypeSkillRequirementList(shiftConverter.shiftTypeSkillRequirements);

        addDayOffRequests(roster, p);
        addDayOnRequests(roster, p);
        addVacationRequests(roster, p);
        roster.setShiftOnRequestList(Lists.newArrayList());
        roster.setShiftOffRequestList(Lists.newArrayList());
        addShiftAssignmentList(roster);

        //todo: some more stuff to sort out
        roster.setPatternList(Lists.newArrayList());
        roster.setPatternContractLineList(Lists.newArrayList());

        return roster;
    }

    private NurseRoster createRoster() {
        NurseRoster nurseRoster = new NurseRoster();
        long id = 1L;
        nurseRoster.setId(id);
        nurseRoster.setCode("nurseRoster-" + id);
        return nurseRoster;
    }

    private void addShiftAssignmentList(NurseRoster nurseRoster) {
        List<org.optaplanner.examples.nurserostering.domain.Shift> shiftList = nurseRoster.getShiftList();
        List<org.optaplanner.examples.nurserostering.domain.ShiftAssignment> shiftAssignmentList = new ArrayList<org.optaplanner.examples.nurserostering.domain.ShiftAssignment>(shiftList.size());
        long id = 0L;
        for (Shift shift : shiftList) {
            for (int i = 0; i < shift.getRequiredEmployeeSize(); i++, id++) {
                shiftAssignmentList.add(getShiftAssignment(id, shift, i));
            }
        }
        nurseRoster.setShiftAssignmentList(shiftAssignmentList);
    }

    private ShiftAssignment getShiftAssignment(long id, Shift shift, int i) {
        ShiftAssignment shiftAssignment = new ShiftAssignment();
        shiftAssignment.setId(id);
        shiftAssignment.setShift(shift);
        shiftAssignment.setIndexInShift(i);
        return shiftAssignment;
    }

    private void addContracts(NurseRoster nurseRoster) {
        nurseRoster.setContractList(Lists.newArrayList(contracts.map.values()));
        nurseRoster.setContractLineList(Lists.newArrayList());
        contracts.map.values().forEach(contract -> {
            nurseRoster.getContractLineList().addAll(contract.getContractLineList());
        });
    }

    private void addVacationRequests(NurseRoster nurseRoster, Period p) {
        this.addEmployeeRequests(nurseRoster, p, Request.Type.VACATION, 50);
    }

    private void addDayOffRequests(NurseRoster nurseRoster, Period p) {
        this.addEmployeeRequests(nurseRoster, p, Request.Type.DAY_OFF, 30);
    }

    //todo: refactor some common logic w/ dayOn request
    private void addDayOnRequests(NurseRoster nurseRoster, Period p) {
        List<org.optaplanner.examples.nurserostering.domain.request.DayOnRequest> list = Lists.newArrayList();
        AtomicLong id = new AtomicLong(0);

        int extraWeight = 10;

        p.request.map.entrySet().stream()
                .filter(e -> e.getKey() == Request.Type.DAY_ON)
                .forEach(e -> e.getValue().forEach((emplId, dates) -> {
                    dates.forEach(dateStr -> {
                        org.optaplanner.examples.nurserostering.domain.request.DayOnRequest request =
                                new org.optaplanner.examples.nurserostering.domain.request.DayOnRequest();
                        ShiftDate shiftDate = shiftDates.map.get(dateStr);
                        request.setId(id.getAndIncrement());
                        Employee employee = this.employees.map.get(emplId);
                        if (employee.getDayOnRequestMap() == null) {
                            employee.setDayOnRequestMap(Maps.newHashMap());
                        }
                        employee.getDayOnRequestMap().put(shiftDate, request);
                        request.setEmployee(employee);
                        request.setShiftDate(shiftDate);
                        int seniority = this.employees.apiMap.get(emplId).getSeniority();
                        final int maxWeight = contracts.getMaxWeight(employee.getContract().getCode());
                        request.setWeight(seniority + maxWeight + extraWeight);
                        list.add(request);
                    });
                }));

        List<DayOnRequest> existing = nurseRoster.getDayOnRequestList();
        if (existing == null) {
            existing = Lists.newArrayList();
            nurseRoster.setDayOnRequestList(existing);
        }
        existing.addAll(list);
    }

    private void addEmployeeRequests(NurseRoster nurseRoster, Period p, Request.Type type, int extraWeight) {
        List<org.optaplanner.examples.nurserostering.domain.request.DayOffRequest> list = Lists.newArrayList();
        AtomicLong id = new AtomicLong(0);

        p.request.map.entrySet().stream()
                .filter(e -> e.getKey() == type)
                .forEach(e -> e.getValue().forEach((emplId, dates) -> {
                    dates.forEach(dateStr -> {
                        ShiftDate shiftDate = shiftDates.map.get(dateStr);
                        Employee employee = this.employees.map.get(emplId);

                        final int seniority = this.employees.apiMap.get(emplId).getSeniority();
                        final int maxWeight = contracts.getMaxWeight(employee.getContract().getCode());
                        final int weight = seniority + maxWeight + extraWeight;

                        list.add(getDayOffRequest(
                                shiftDate, id.getAndIncrement(),
                                employee, dayOffTypeMapping.get(type), weight));
                    });
                }));

        List<DayOffRequest> existing = nurseRoster.getDayOffRequestList();
        if (existing == null) {
            existing = Lists.newArrayList();
            nurseRoster.setDayOffRequestList(existing);
        }
        existing.addAll(list);
    }

    private DayOffRequest getDayOffRequest(ShiftDate shiftDate, long nextId,
                                           Employee employee,
                                           DayOffRequest.Type requestType,
                                           int weight) {
        DayOffRequest request =
                new DayOffRequest();
        request.setId(nextId);
        if (employee.getDayOffRequestMap() == null) {
            employee.setDayOffRequestMap(Maps.newHashMap());
        }
        employee.getDayOffRequestMap().put(shiftDate, request);
        request.setEmployee(employee);
        request.setType(requestType);
        request.setShiftDate(shiftDate);
        request.setWeight(weight);
        return request;
    }

    private void addNurseRosterInfo(NurseRoster nurseRoster) {
        List<ShiftDate> list = nurseRoster.getShiftDateList();
        ShiftDate start = list.get(0);
        ShiftDate end = list.get(list.size() - 1);

        NurseRosterParametrization parametrization = new NurseRosterParametrization();
        parametrization.setFirstShiftDate(start);
        parametrization.setLastShiftDate(end);
        parametrization.setPlanningWindowStart(start);

        nurseRoster.setNurseRosterParametrization(parametrization);
    }

}
