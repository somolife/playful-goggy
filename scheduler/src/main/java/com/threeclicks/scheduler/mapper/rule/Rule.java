package com.threeclicks.scheduler.mapper.rule;

import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.domain.ShiftAssignment;

import java.util.function.Function;
import java.util.stream.Stream;

/**
 * need to support the concept of a set of constraint/assertions that's applied on employee assignment
 * technically they're selectors/predicates)
 * <p>
 * for each employee,
 * given a Contract/minMax and assignments, assert min, max met
 * given an UnwantedPattern and assignments, assert pattern was not matched
 * for each day, given a CoverDimenision and assignments, assert coverDimension met
 *
 * @author nigeldev, @date 3/22/18
 */
public abstract class Rule<T, R> implements Function<T, R> {
    private final String name;

    Rule(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    Stream<ShiftAssignment> getAssignments(NurseRoster roster) {
        return roster.getShiftAssignmentList().stream()
                .filter(assignment -> assignment.getEmployee() != null);
    }

}
