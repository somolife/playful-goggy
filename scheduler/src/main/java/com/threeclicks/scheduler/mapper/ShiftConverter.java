package com.threeclicks.scheduler.mapper;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.threeclicks.scheduler.api.cover.CompositeCover;
import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.cover.CoverRequirement;
import org.optaplanner.examples.nurserostering.domain.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * @author nigeldev, @date 3/13/18
 */
//todo: add skill requirement here --> ShiftTypeSkillRequirement
class ShiftConverter {
    final List<ShiftType> shiftTypes;
    final Map<String, ShiftType> shiftTypeMap = Maps.newHashMap();
    final List<Shift> shifts;
    final List<ShiftTypeSkillRequirement> shiftTypeSkillRequirements = Lists.newArrayList();

    final Map<String, Skill> skillMap;

    ShiftConverter(CompositeCover cover, List<ShiftDate> shiftDates, Set<Skill> skills) {
        this.skillMap = skills.stream().collect(Collectors.toMap(o -> o.getCode(), o -> o));
        this.shiftTypes = getShiftTypes(cover);
        this.shifts = Lists.newArrayList(getShifts(cover, shiftDates));
    }

    private Collection<Shift> getShifts(CompositeCover cover, List<ShiftDate> shiftDates) {
        final AtomicLong id = new AtomicLong(0);
        final AtomicInteger index = new AtomicInteger(0);
        final Map<Long, Shift> shiftMap = Maps.newHashMap();

        for (ShiftDate shiftDate : shiftDates) {
            cover.map.entrySet().stream()
                    .filter(entry -> getCover(entry.getValue(), shiftDate) > 0)
                    .forEach(entry -> {
                        Shift shift = getShift(id.incrementAndGet(), index.incrementAndGet(), shiftDate);
                        shift.setShiftType(getShiftType(entry));
                        shift.setRequiredEmployeeSize(getCover(entry.getValue(), shiftDate));
                        shiftMap.put(shift.getId(), shift);

                    });
        }
        return shiftMap.values();
    }

    private ShiftType getShiftType(Map.Entry<CompositeKey, CoverRequirement> entry) {
        return shiftTypeMap.get(entry.getKey().toJson());
    }

    private Shift getShift(long id, int index, ShiftDate shiftDate) {
        Shift shift = new Shift();
        shift.setIndex(index);
        shift.setId(id);
        shift.setShiftDate(shiftDate);
        shiftDate.getShiftList().add(shift);
        return shift;
    }

    private int getCover(CoverRequirement wrapped, ShiftDate shiftDate) {
        return wrapped.getCover(shiftDate.getDayOfWeek().ordinal(), shiftDate.getDateString());
    }

    private List<ShiftType> getShiftTypes(CompositeCover cover) {
        AtomicLong shiftTypeId = new AtomicLong(0);
        AtomicLong skillRequirementId = new AtomicLong(0);
        return cover.map.keySet().stream()
                .map(dimension -> {
                    ShiftType type = new ShiftType();
                    final String code = dimension.toJson();
                    type.setCode(code);
                    shiftTypeMap.put(code, type);
                    final ShiftTypeSkillRequirement skillRequirement = new ShiftTypeSkillRequirement();
                    skillRequirement.setShiftType(type);
                    final String skillCode = dimension.types.get(CompositeKey.Type.SKILL);
                    final Skill skill = skillMap.get(skillCode);
                    skillRequirement.setSkill(skill);
                    skillRequirement.setId(skillRequirementId.incrementAndGet());
                    shiftTypeSkillRequirements.add(skillRequirement);
                    type.setDescription("Day");
                    type.setId(shiftTypeId.incrementAndGet());
                    type.setNight(false);
                    type.setStartTimeString("08:00:00");
                    type.setEndTimeString("20:30:00");
                    return type;
                }).collect(Collectors.toList());
    }
}
