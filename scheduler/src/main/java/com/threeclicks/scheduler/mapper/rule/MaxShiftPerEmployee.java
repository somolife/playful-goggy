package com.threeclicks.scheduler.mapper.rule;

import org.optaplanner.examples.nurserostering.domain.contract.MinMaxContractLine;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author nigeldev, @date 3/22/18
 */
public class MaxShiftPerEmployee extends MinMaxPerEmployee {

    public MaxShiftPerEmployee() {
        super(MaxShiftPerEmployee.class.getSimpleName());
    }

    List getList(Map<Long, Long> employeeAssignmentMap, Map<Long, MinMaxContractLine> lineMap) {
        return lineMap.entrySet().stream()
                .filter(e -> !employeeAssignmentMap.containsKey(e.getKey()) ||
                        employeeAssignmentMap.get(e.getKey()) > e.getValue().getMaximumValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
