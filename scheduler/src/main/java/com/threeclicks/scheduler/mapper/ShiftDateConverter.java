package com.threeclicks.scheduler.mapper;

import com.google.common.collect.Lists;
import com.threeclicks.scheduler.api.DateRange;
import com.threeclicks.scheduler.api.Period;
import org.assertj.core.util.Maps;
import org.optaplanner.examples.nurserostering.domain.DayOfWeek;
import org.optaplanner.examples.nurserostering.domain.ShiftDate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author nigeldev, @date 3/7/18
 */
class ShiftDateConverter {
    final Map<String, ShiftDate> map;
    final List<ShiftDate> dates;

    ShiftDateConverter(Period p) {
        this.map = Maps.newHashMap();
        this.dates = Lists.newArrayList();
        this.addShiftDates(p);
    }

    private void addShiftDates(Period p) {
        long id = 0;
        int dayIndex = 0;

        for(LocalDate date : p.dateRange.stream().collect(Collectors.toList())){
            ShiftDate shiftDate = new ShiftDate();
            shiftDate.setId(id);
            shiftDate.setDayIndex(dayIndex);
            String dateString = DateRange.formatter.format(date);
            shiftDate.setDateString(dateString);
            shiftDate.setDayOfWeek(DayOfWeek.valueOfCode(date.getDayOfWeek().name()));
            shiftDate.setShiftList(new ArrayList<>());
            map.put(dateString, shiftDate);
            this.dates.add(shiftDate);
            id++;
            dayIndex++;
        }
    }
}
