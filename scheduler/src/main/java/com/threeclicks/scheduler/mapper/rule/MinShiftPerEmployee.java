package com.threeclicks.scheduler.mapper.rule;

import org.optaplanner.examples.nurserostering.domain.contract.MinMaxContractLine;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author nigeldev, @date 3/22/18
 */
public class MinShiftPerEmployee extends MinMaxPerEmployee {

    public MinShiftPerEmployee() {
        super(MinShiftPerEmployee.class.getSimpleName());
    }

    @Override
    List getList(Map<Long, Long> employeeAssignmentMap, Map<Long, MinMaxContractLine> lineMap) {
        return lineMap.entrySet().stream()
                .filter(e -> !employeeAssignmentMap.containsKey(e.getKey()) ||
                        employeeAssignmentMap.get(e.getKey()) < e.getValue().getMinimumValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
