package com.threeclicks.scheduler.api.cover;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.threeclicks.scheduler.api.ShiftCover;

/**
 * how many folks do I need ...
 *
 * @author nigeldev, @date 3/16/18
 */
public interface CoverRequirement {

    /**
     * in total
     *
     * @return
     */
    int numCovers();

    /**
     * for a particular day
     *
     * @param weekday
     * @param dateStr
     * @return
     */
    int getCover(int weekday, String dateStr);

}
