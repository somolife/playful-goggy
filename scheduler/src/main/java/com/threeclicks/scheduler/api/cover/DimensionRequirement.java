package com.threeclicks.scheduler.api.cover;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.ShiftCover;

/**
 * e.g. {dimension: {skill: nurse, location=NY}, requirement: {...}}
 *
 * @author nigeldev, @date 3/16/18
 */
public class DimensionRequirement implements CoverRequirement {
    @JsonProperty
    public final CompositeKey dimension;
    @JsonProperty
    public final ShiftCover requirement;

    public DimensionRequirement(@JsonProperty("requirement") ShiftCover requirement,
                                @JsonProperty("dimension") CompositeKey dimension) {
        this.requirement = requirement;
        this.dimension = dimension;
    }

    @Override
    public int numCovers() {
        return this.requirement.numCovers();
    }

    @Override
    public int getCover(int weekday, String dateStr) {
        return this.requirement.getCover(weekday, dateStr);
    }
}
