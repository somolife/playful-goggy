package com.threeclicks.scheduler.api.cover;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Sets;
import com.threeclicks.scheduler.api.CompositeKey;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

/**
 * Is not serialized over the wire
 *
 * @author nigeldev, @date 3/16/18
 */
public class CompositeCover {
    @JsonIgnore
    public Map<CompositeKey, CoverRequirement> map;
    @JsonProperty
    Set<DimensionRequirement> composite;

    @SuppressWarnings("unchecked")
    public static CompositeCover create(DimensionRequirement... requirements) {
        final HashSet<DimensionRequirement> ts = Sets.newHashSet(requirements);
        return new CompositeCover(ts);
    }

    @JsonCreator
    public CompositeCover(@JsonProperty("composite") Set<DimensionRequirement> composite) {
        this.composite = composite;
        this.map = composite.stream().collect(toMap(o -> o.dimension, o -> o.requirement));
    }

    public CoverRequirement get(CompositeKey dimension) {
        return map.get(dimension);
    }
}
