package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.annotation.*;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

/**
 * lots of constraints for the employee would be mapped here
 * DEFAULT assumes a 28-day period that is prorated
 * todo: add {@link com.threeclicks.scheduler.api.pattern.CoverDimensionFollowedByFreeDay}
 *
 * @author nigeldev, @date 2/15/18 9:51 AM
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Contract {
    public static final String DEFAULT_CODE = "DEF";

    public final String code; //primaryKey
    public final String weekendDefnCode = "SaturdaySunday";

    public final WeightedValue<Boolean> alternativeSkill;
    public final WeightedValue<Boolean> singleAssignmentPerDay;
    public final WeightedValue<Boolean> identicalShiftTypesDuringWeekend;

    public final MinMax totalAssignments;
    public final MinMax consecutiveWorkingDays;
    public final MinMax consecutiveFreeDays;
    public final MinMax consecutiveWorkingWeekends;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contract contract = (Contract) o;
        return Objects.equal(code, contract.code);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(code);
    }

    public static final int DEFAULT_BASIS = 28;
    private static final Contract defaultContractInstance = new Contract(
            DEFAULT_CODE,
            new WeightedValue<>(true, 60),
            new WeightedValue<>(true, 50),
            new WeightedValue<>(true, 40),
            new MinMax(
                    new WeightedValue<>((DEFAULT_BASIS * 2) / 3, 20),
                    new WeightedValue<>((DEFAULT_BASIS * 2) / 3, 30)),
            new MinMax(new WeightedValue<>(2, 15), new WeightedValue<>(3, 10)),
            new MinMax(new WeightedValue<>(2, 6), new WeightedValue<>(2, 5)),
            new MinMax(new WeightedValue<>(1, 3), new WeightedValue<>(2, 2))
    );

    public static final Contract defaultContract() {
        return defaultContractInstance;
    }

    @JsonCreator
    public Contract(@JsonProperty("code") String code,
                    @JsonProperty("alternativeSkill") WeightedValue<Boolean> alternativeSkill,
                    @JsonProperty("singleAssignmentPerDay") WeightedValue<Boolean> singleAssignmentPerDay,
                    @JsonProperty("identicalShiftTypesDuringWeekend") WeightedValue<Boolean> identicalShiftTypesDuringWeekend,
                    @JsonProperty("totalAssignments") MinMax totalAssignments,
                    @JsonProperty("consecutiveWorkingDays") MinMax consecutiveWorkingDays,
                    @JsonProperty("consecutiveFreeDays") MinMax consecutiveFreeDays,
                    @JsonProperty("consecutiveWorkingWeekends") MinMax consecutiveWorkingWeekends) {
        this.code = code;
        this.alternativeSkill = alternativeSkill;
        this.singleAssignmentPerDay = singleAssignmentPerDay;
        this.identicalShiftTypesDuringWeekend = identicalShiftTypesDuringWeekend;

        this.totalAssignments = totalAssignments;
        this.consecutiveWorkingDays = consecutiveWorkingDays;
        this.consecutiveFreeDays = consecutiveFreeDays;
        this.consecutiveWorkingWeekends = consecutiveWorkingWeekends;
    }

    @JsonIgnore
    public int maxWeight() {
        return Lists.newArrayList(
                this.alternativeSkill, this.singleAssignmentPerDay, this.identicalShiftTypesDuringWeekend,
                this.totalAssignments, this.consecutiveWorkingDays, this.consecutiveFreeDays,
                this.consecutiveWorkingWeekends)
                .stream()
                .map(MaxWeight::getMaxWeight)
                .reduce(0, Math::max);
    }

    interface MaxWeight {
        int getMaxWeight();
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public static class MinMax implements MaxWeight {
        public final WeightedValue<Integer> min;
        public final WeightedValue<Integer> max;

        @JsonCreator
        public MinMax(@JsonProperty("min") WeightedValue<Integer> min,
                      @JsonProperty("max") WeightedValue<Integer> max) {
            this.min = min;
            this.max = max;
        }

        @Override
        @JsonIgnore
        public int getMaxWeight() {
            return Math.max(min.weight, max.weight);
        }
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public static class WeightedValue<V> implements MaxWeight {
        public final int weight;
        public final V value;

        @JsonCreator
        public WeightedValue(@JsonProperty("value") V value,
                             @JsonProperty("weight") int weight) {
            this.weight = weight;
            this.value = value;
        }

        @Override
        @JsonIgnore
        public int getMaxWeight() {
            return this.weight;
        }
    }

}
