package com.threeclicks.scheduler.api;

import org.assertj.core.util.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.threeclicks.scheduler.api.Employee.resetIdGen;

/**
 * @author nigeldev, @date 2/16/18
 */
public class Employees {


    public static List<Employee> createEmployeesFromScratch(int n) {
        resetIdGen();
        return createEmployees(n);
    }

    public static List<Employee> createEmployees(int n) {
        List<Employee> l = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            Employee e = new Employee();
            e.setContractCode(Contract.defaultContract().code);
            l.add(e);
        }
        return l;
    }

    public static Map<Long, Employee> myById(List<Employee> list) {
        Map<Long, Employee> map = Maps.newHashMap();
        list.forEach(employee -> map.put(employee.getId(), employee));
        return map;
    }
}
