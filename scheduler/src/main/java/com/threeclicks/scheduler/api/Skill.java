package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author nigeldev, @date 3/16/18
 */
public class Skill {
    @JsonProperty
    final String code;

    @JsonCreator
    Skill(@JsonProperty("code") String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Skill skill = (Skill) o;
        return Objects.equal(code, skill.code);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(code);
    }

}
