package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author nigeldev, @date 3/16/18
 */
public class Location {
    @JsonProperty
    final String code;

    @JsonCreator
    Location(@JsonProperty("code") String code) {
        this.code = code;
    }

    public String code() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equal(code, location.code);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(code);
    }
}
