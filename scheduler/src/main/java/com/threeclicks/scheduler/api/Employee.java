package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import java.util.concurrent.atomic.AtomicLong;

/**
 * should only be concerned with fairly immutable data of the employee (e.g. name)
 *
 * @author nigeldev, @date 2/15/18 9:51 AM
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Employee {
    public static final String I_DO_IT_ALL = "JackOfAll"; //todo: right this ship asaps!

    private static final AtomicLong longGen = new AtomicLong(1); //todo: get a proper ID asap

    @JsonIgnore
    public static void resetIdGen() { //todo: this is hell waiting to happen, fix it with a proper ID in this class
        longGen.set(1L);
    }

    @JsonProperty
    private String contractCode = Contract.DEFAULT_CODE;    // link to a contract defn

    @JsonProperty //start off simple w/ just 1 code per employee
    private String skillCode = I_DO_IT_ALL;    // link to a code defn

    @JsonProperty
    private int seniority = 1;

    @JsonProperty
    private final long id = longGen.getAndIncrement(); //primaryKey -- really for eq/hash

    @JsonProperty
    private String name = "Empl-" + id;

    Employee() {

    }

    public long getId() {
        return id;
    }

    public int getSeniority() {
        return seniority;
    }

    public void setSeniority(int seniority) {
        this.seniority = seniority;
    }

    public static AtomicLong getLongGen() {
        return longGen;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getSkillCode() {
        return skillCode;
    }

    public void setSkillCode(String skillCode) {
        this.skillCode = skillCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
