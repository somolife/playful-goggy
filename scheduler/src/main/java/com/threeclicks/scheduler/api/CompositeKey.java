package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * dimensions e.g. skill=nurse, location=NY, ...
 * This comes in over the wire as a map
 *
 * @author nigeldev, @date 3/16/18
 */
public class CompositeKey {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    @JsonProperty
    public final Map<Type, String> types;

    @JsonCreator
    public CompositeKey(@JsonProperty("types") Map<Type, String> types) {
        this.types = types;
    }

    public static CompositeKey forSkill(String skill) {
        return new CompositeKey(ImmutableMap.of(Type.SKILL, skill));
    }

    public static CompositeKey forLocation(String location) {
        return new CompositeKey(ImmutableMap.of(
                Type.LOCATION, location));
    }

    public static CompositeKey forLocationAndSkill(String location, String skill) {
        return new CompositeKey(ImmutableMap.of(
                Type.SKILL, skill,
                Type.LOCATION, location));
    }

    private static Map<String, String> fromJson(String json) {
        try {
            return MAPPER.readValue(json, Map.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @JsonIgnore
    public static String valueFromJson(Type type, String json) {
        return fromJson(json).get(type.toString());
    }

    private static String getString(Object o) {
        try {
            return MAPPER.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @JsonIgnore
    public String toJson() {
        return getString(this.types);
    }

    @JsonIgnore
    public boolean partialMatch(String json) {
        final Map<String, String> map = fromJson(json);
        return this.types.entrySet().stream()
                .filter(e -> map.containsKey(e.getKey().name()))
                .allMatch(e -> e.getValue().equals(map.get(e.getKey().name())));
    }

//    private static Map<Type, String> convert(Map<String, String> map) {
//        return map.entrySet().stream()
//                .collect(toMap(e -> Type.valueOf(e.getKey()), Map.Entry::getValue));
//    }

    @Override
    @JsonIgnore
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeKey that = (CompositeKey) o;
        return Objects.equal(types, that.types);
    }

    @Override
    @JsonIgnore
    public int hashCode() {
        return Objects.hashCode(types);
    }

    @Override
    public String toString() {
        return "CompositeKey{" +
                "types=" + types +
                '}';
    }

    public enum Type {
        SKILL, LOCATION, EMPLOYEE, DAY_OF_WEEK
    }
}
