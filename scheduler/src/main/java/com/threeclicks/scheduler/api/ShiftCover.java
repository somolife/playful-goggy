package com.threeclicks.scheduler.api;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.threeclicks.scheduler.api.cover.CoverRequirement;
import org.assertj.core.util.Lists;

import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

/**
 * lightweight APi version
 * should support 3 configs from low -> high priority
 * - default
 * - day of week
 * - specific date e.g. a holiday
 * @author nigeldev, @date 3/13/18
 */

public class ShiftCover implements CoverRequirement {

    @JsonProperty
    private int defaultCover;
    @JsonProperty
    private List<DayOfWeekCover> dayOfWeekCovers;
    @JsonProperty
    private List<SpecficDateCover> specificDateCovers;

    @JsonCreator
    public ShiftCover(@JsonProperty("defaultCover") int defaultCover,
                      @JsonProperty("dayOfWeekCovers") List<DayOfWeekCover> dayOfWeekCovers,
                      @JsonProperty("specificDateCovers") List<SpecficDateCover> specificDateCovers) {
        this.defaultCover = defaultCover;
        this.dayOfWeekCovers = dayOfWeekCovers == null ? Lists.newArrayList() : dayOfWeekCovers;
        this.specificDateCovers = specificDateCovers == null ? Lists.newArrayList() : specificDateCovers;
    }

    public ShiftCover(int defaultCover) {
        this.defaultCover = defaultCover;
        this.dayOfWeekCovers = Lists.newArrayList();
        this.specificDateCovers = Lists.newArrayList();
    }

    @JsonIgnore
    public int numCovers() {
        return specificDateCovers.size() + dayOfWeekCovers.size() + 1;
    }

    @JsonIgnore
    public int getCover(int weekday, String dateStr) {
        return specificDateCovers.stream()
                .filter(specficDateCover1 -> specficDateCover1.test(dateStr))
                .findAny().map(specficDateCover -> specficDateCover.cover)
                .orElseGet(() -> dayOfWeekCovers.stream()
                        .filter(dayOfWeekCover1 -> dayOfWeekCover1.test(weekday))
                        .findAny().map(dayOfWeekCover -> dayOfWeekCover.cover)
                        .orElse(defaultCover));
    }

    public static class DayOfWeekCover implements IntPredicate {
        @JsonProperty
        int weekday; //MONDAY is 1
        @JsonProperty
        final int cover;

        @JsonCreator
        public DayOfWeekCover(@JsonProperty("weekday") int weekday,
                              @JsonProperty("cover") int cover) {
            this.weekday = weekday;
            this.cover = cover;
        }

        @Override
        public boolean test(int i) {
            return this.weekday == i;
        }
    }

    public static class SpecficDateCover implements Predicate<String> {
        @JsonProperty
        String dateStr;
        @JsonProperty
        final int cover;

        @JsonCreator
        public SpecficDateCover(@JsonProperty("date") String dateStr,
                                @JsonProperty("cover") int cover) {
            this.dateStr = dateStr;
            this.cover = cover;
        }

        @Override
        public boolean test(String s) {
            return this.dateStr.equals(s);
        }
    }

    @Override
    public String toString() {
        return "ShiftCover{" +
                "defaultCover=" + defaultCover +
                ", dayOfWeekCovers=" + dayOfWeekCovers +
                ", specificDateCovers=" + specificDateCovers +
                '}';
    }
}
