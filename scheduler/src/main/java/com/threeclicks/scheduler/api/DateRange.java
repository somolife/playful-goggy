package com.threeclicks.scheduler.api;

import com.google.common.base.Objects;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Need to key into a list of periods, figure this would be a simple effective organization tool
 * <p>
 * create next/previous range
 * check range contains date
 * <p>
 *
 * @author nigeldev, @date 3/9/18
 */
public class DateRange {
    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static final Comparator<DateRange> START_DATE_COMPARATOR = (d1, d2) -> {
        if (d1.equals(d2)) return 0;
        if (d1.startDate.isAfter(d2.startDate)) return 1;
        return -1;
    };

    public final int numDays;
    public final String startDateStr;

    public final LocalDate startDate;
    public final LocalDate endDate;

    DateRange(String startDateStr, int numDays) {
        if (startDateStr == null) {
            this.startDate = LocalDate.now();
            this.startDateStr = startDate.format(formatter);
        } else {
            this.startDateStr = startDateStr;
            this.startDate = LocalDate.parse(startDateStr);
        }
        this.numDays = numDays;
        this.endDate = this.startDate.plusDays(numDays - 1);
    }

    public Stream<LocalDate> stream() {
        return IntStream.range(0, this.numDays).mapToObj(this.startDate::plusDays);
    }

    public DateRange previous() {
        LocalDate localDate = this.startDate.minusDays(this.numDays);
        String format = localDate.format(formatter);
        return new DateRange(format, this.numDays);
    }

    public DateRange next() {
        LocalDate localDate = this.endDate.plusDays(1);
        String format = localDate.format(formatter);
        return new DateRange(format, this.numDays);
    }

    public boolean contains(LocalDate date) {
        return date.isAfter(this.startDate.minusDays(1)) && date.isBefore(this.endDate.plusDays(1));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateRange dateRange = (DateRange) o;
        return numDays == dateRange.numDays &&
                Objects.equal(startDateStr, dateRange.startDateStr);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(startDateStr, numDays);
    }
}
