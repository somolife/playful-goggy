package com.threeclicks.scheduler.api.pattern;

import com.threeclicks.scheduler.api.CompositeKey;

/**
 * @author nigeldev, @date 3/26/18
 */
public class CoverDimensionFollowedByFreeDay {

    private CompositeKey compositeKey;

    public CoverDimensionFollowedByFreeDay(CompositeKey compositeKey) {
        this.compositeKey = compositeKey;
    }

    public CompositeKey getCompositeKey() {
        return compositeKey;
    }
}
