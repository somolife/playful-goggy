package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.annotation.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * @author nigeldev, @date 2/22/18
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Request {

    public enum Type {
        DAY_OFF,
        DAY_ON,
        VACATION
    }

    @JsonProperty
    public final Map<Type, Map<Long, List<String>>> map;

    @JsonCreator
    public Request(@JsonProperty("map") Map<Type, Map<Long, List<String>>> map) {
        this.map = map;
    }

    Request() {
        this.map = Maps.newHashMap();
    }

    void add(Type type, Long emplId, List<String> dateStrs) {
        Map<Long, List<String>> empMap = map.computeIfAbsent(type, t -> Maps.newHashMap());
        List<String> dates = empMap.computeIfAbsent(emplId, id -> Lists.newArrayList());
        dates.addAll(dateStrs);
    }

    @JsonIgnore
    public List<String> get(Type type, Long emplId) {
        return map.get(type).get(emplId);
    }

}
