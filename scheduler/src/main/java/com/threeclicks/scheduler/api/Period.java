package com.threeclicks.scheduler.api;


import com.fasterxml.jackson.annotation.*;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.threeclicks.scheduler.api.cover.CompositeCover;
import com.threeclicks.scheduler.api.cover.CoverRequirement;
import com.threeclicks.scheduler.api.cover.DimensionRequirement;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import static com.threeclicks.scheduler.api.Employee.I_DO_IT_ALL;

/**
 * work period, e.g. 1 week, 1 month ...
 * Created by nigeldev on 2/15/18.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Period {
    static final int STANDARD_WK = 7;
    private static final CompositeKey DEFAULT_SKILL_DIMENSION =
            new CompositeKey(ImmutableMap.of(CompositeKey.Type.SKILL, I_DO_IT_ALL));

    @JsonProperty
    public long id = 1; //yes, a crappy id, but it's a start; todo: better id's y'all
    @JsonProperty
    public int numDays; //in days
    @JsonProperty
    public int shiftCoverPerDay; // this gets baked into the shiftCover
    @JsonProperty
    public int numEmployees;

    //todo: split out these ignorable components
    @JsonIgnore
    public List<Employee> employees;
    @JsonIgnore
    public Collection<Contract> contracts;
    @JsonIgnore
    public Request request;
    @JsonIgnore
    public DateRange dateRange;

    @JsonIgnore
    private CompositeCover shiftCover;

    @JsonCreator
    Period(@JsonProperty("numDays") int numDays,
           @JsonProperty("shiftCoverPerDay") int shiftsPerDay,
           @JsonProperty("startDate") String startDateStr,
           @JsonProperty("numEmployees") int numEmployees) {
        this.numDays = numDays;
        this.shiftCoverPerDay = shiftsPerDay;
        this.numEmployees = numEmployees;
        this.dateRange = new DateRange(startDateStr, this.numDays);
    }

    public CompositeCover getShiftCover() {
        return shiftCover;
    }

    @JsonIgnore
    public CoverRequirement getDefaultShiftCover() {
        return shiftCover.get(DEFAULT_SKILL_DIMENSION);
    }


    public void setShiftCover(CompositeCover shiftCover) {
        this.shiftCover = shiftCover;
    }

    //todo: remove temp hack once skill sorted out
    @JsonIgnore
    public void setDefaultSkillShiftCover(ShiftCover shiftCover) {
        this.shiftCover = getCompositeCover(shiftCover);
    }

    @JsonGetter("startDate")
    public String getStartDateStr() {
        return this.dateRange.startDateStr;
    }

    @JsonSetter("startDate")
    public void setStartDateStr(String startDateStr) {
        this.dateRange = new DateRange(startDateStr, this.numDays);
    }

    @JsonIgnore
    public void setContracts(Collection<Contract> contracts) {
        this.contracts = contracts;
    }

    @JsonIgnore
    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @JsonIgnore
    public void setRequest(Request request) {
        this.request = request;
    }

    @JsonProperty("id")
    public void setId(long id) {
        this.id = id;
    }

    @JsonIgnore
    public Period next() {
        return buildClone(this.numDays, shiftCoverPerDay, this.dateRange.next().startDateStr, numEmployees, shiftCover);
    }

    private static Period buildClone(int numDays,
                                     int shiftCoverPerDay,
                                     String startDateStr,
                                     int numEmployees,
                                     CompositeCover shiftCover) {
        final Period period = new Period(numDays, shiftCoverPerDay, startDateStr, numEmployees);
        period.setShiftCover(shiftCover);
        return period;
    }

    @JsonIgnore
    public Period previous() {
        return buildClone(this.numDays, shiftCoverPerDay,
                this.dateRange.previous().startDateStr,
                numEmployees, shiftCover);
    }

    @JsonIgnore
    public boolean containsDate(LocalDate date) {
        return this.dateRange.contains(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Period period = (Period) o;
        return Objects.equal(dateRange, period.dateRange);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(dateRange);
    }

    public static class Builder {
        int duration;
        int shiftsPerDay = 1;
        String startDate;
        int numEmployees;

        //additional stuff, useful for testing
        List<ShiftCover.SpecficDateCover> dateCover = Lists.newArrayList();
        List<ShiftCover.DayOfWeekCover> dayOfWeekCover = Lists.newArrayList();

        List<Employee> employees = Lists.newArrayList();
        Collection<Contract> contracts;
        Request request = new Request();

        public Builder numDays(int numDays) {
            this.duration = numDays;
            return this;
        }

        public Builder forOneWeek() {
            this.duration = STANDARD_WK;
            return this;
        }

        public Builder forOneMonth() {
            this.duration = STANDARD_WK * 4;
            return this;
        }

        public Builder startDate(String startDate) {
            this.startDate = startDate;
            return this;
        }

        //todo: set cover for cover dimensions e.g. skill
        public Builder shiftCoverPerDay(int shiftsPerDay) {
            this.shiftsPerDay = shiftsPerDay;
            return this;
        }

        public Builder holidayCover(int cover, String... holidays) {
            for (String h : holidays) {
                this.dateCover.add(new ShiftCover.SpecficDateCover(h, cover));
            }
            return this;
        }

        public Builder dayOfWeekCover(int cover, Integer... days) {
            for (Integer d : days) {
                this.dayOfWeekCover.add(new ShiftCover.DayOfWeekCover(d, cover));
            }
            return this;
        }

        public Builder employees(List<Employee> employees) {
            this.employees = employees;
            return numEmployees(employees.size());
        }

        public Builder numEmployees(int num) {
            this.numEmployees = num;
            return this;
        }

        public Builder contracts(Collection<Contract> contracts) {
            this.contracts = contracts;
            return this;
        }

        public Builder daysOff(int emplIndex, List<String> dates) {
            request.add(Request.Type.DAY_OFF, this.employees.get(emplIndex).getId(), dates);
            return this;
        }

        public Builder daysOn(int emplIndex, List<String> dates) {
            request.add(Request.Type.DAY_ON, this.employees.get(emplIndex).getId(), dates);
            return this;
        }

        public Builder vacations(int emplIndex, List<String> dates) {
            request.add(Request.Type.VACATION, this.employees.get(emplIndex).getId(), dates);
            return this;
        }

        public Period build() {
            Period period = new Period(
                    this.duration,
                    this.shiftsPerDay,
                    this.startDate,
                    this.numEmployees
            );
            period.setContracts(contracts);
            period.setRequest(request);
            period.setEmployees(employees);
            period.setShiftCover(mergeShiftCovers());

            return period;
        }

        private CompositeCover mergeShiftCovers() {
            final ShiftCover cover = new ShiftCover(this.shiftsPerDay, this.dayOfWeekCover, this.dateCover);
            return getCompositeCover(cover);
        }
    }

    private static CompositeCover getCompositeCover(ShiftCover cover) {
        return CompositeCover.create(new DimensionRequirement(cover, DEFAULT_SKILL_DIMENSION));
    }
}
