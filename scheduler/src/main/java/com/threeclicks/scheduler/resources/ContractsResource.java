package com.threeclicks.scheduler.resources;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.threeclicks.scheduler.api.Contract;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Map;

import static com.threeclicks.scheduler.resources.ResourceUtils.notFoundWrapper;

@Path("/contracts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ContractsResource {

    private final Map<String, Contract> store;

    public ContractsResource() {
        this.store = Maps.newConcurrentMap();
        Contract contract = Contract.defaultContract();
        this.store.put(contract.code, contract);
    }

    @GET
    public Map<String, Contract> getAll() throws IOException {
        return ImmutableMap.copyOf(store);
    }

    @DELETE
    public void deleteAll() {
        this.store.clear();
    }

    @POST
    @Path("/{id}")
    public void post(@PathParam("id") String id, Contract contract) throws IOException {
        assert contract.code.equals(id);
        store.put(id, contract);
    }

    @GET
    @Path("/{id}")
    public Contract get(@PathParam("id") String id) throws IOException {
        return notFoundWrapper(store.get(id));
    }
}