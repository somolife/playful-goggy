package com.threeclicks.scheduler.resources;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.threeclicks.scheduler.api.Employee;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Map;

import static com.threeclicks.scheduler.resources.ResourceUtils.notFoundWrapper;

@Path("/employees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployeesResource {

    private Map<Long, Employee> store = Maps.newConcurrentMap();

    @GET
    public Map<Long, Employee> getAll() throws IOException {
        return ImmutableMap.copyOf(store);
    }

    @DELETE
    public void deleteAll() {
        this.store.clear();
        Employee.resetIdGen();
    }

    @POST
    @Path("/{id}")
    public void post(@PathParam("id") Long id, Employee employee) throws IOException {
        store.put(id, employee);
    }

    @GET
    @Path("/{id}")
    public Employee get(@PathParam("id") Long id) throws IOException {
        return notFoundWrapper(store.get(id));
    }
}