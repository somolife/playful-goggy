package com.threeclicks.scheduler.resources;

import javax.ws.rs.NotFoundException;
import java.util.Optional;

/**
 * @author nigeldev, @date 3/11/18
 */
class ResourceUtils {
    static <T> T notFoundWrapper(T t) {
        return Optional.ofNullable(t).orElseThrow(NotFoundException::new);
    }
}
