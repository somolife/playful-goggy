package com.threeclicks.scheduler.resources;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.threeclicks.scheduler.api.Period;
import com.threeclicks.scheduler.mapper.EmployeeSchedule;
import com.threeclicks.scheduler.mapper.RosterConverter;
import com.threeclicks.scheduler.mapper.ScheduleConflict;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.config.constructionheuristic.ConstructionHeuristicType;
import org.optaplanner.core.config.solver.EnvironmentMode;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.solver.SolverBuilder;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Map;

import static com.threeclicks.scheduler.resources.ResourceUtils.notFoundWrapper;

@Path("/schedules")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SchedulesResource {

    @Inject
    private EmployeesResource employeesResource;

    /**
     * todo: hack: this being used for checking conflicts
     * todo: instead: rework flow to pass in constraintResource here
     */
    private Map<Long, NurseRoster> store;
    private Map<Long, Period> pStore;

    public SchedulesResource() {
        this.store = Maps.newHashMap();
        this.pStore = Maps.newHashMap();
    }

    @POST
    @Path("/{id}")
    public void post(@PathParam("id") Long id, Period period) throws IOException {
        NurseRoster roster = new RosterConverter().convert(period);
        final long seed = (long) Math.floor(Math.random() * 10657L);
        Solver<Solution> solver = new SolverBuilder()
                .timeLimit(5)
                .envMode(EnvironmentMode.REPRODUCIBLE)
                .random(seed)
                .constructionHeuristic(ConstructionHeuristicType.FIRST_FIT_DECREASING)
                .build();
        Solution solution = solver.solve(roster);
        pStore.put(id, period);
        store.put(id, (NurseRoster) solution);
    }

    @GET
    @Path("/{id}")
    public EmployeeSchedule get(@PathParam("id") Long id) throws IOException {
        NurseRoster roster = notFoundWrapper(store.get(id));
        return EmployeeSchedule.from(roster, employeesResource.getAll());
    }

    @GET
    @Path("/{id}/conflicts")
    public Map getConflicts(@PathParam("id") Long id) throws IOException {
        final Period p = notFoundWrapper(pStore.get(id));
        final NurseRoster roster = notFoundWrapper(store.get(id));
        return new ScheduleConflict(p).asBean(roster);
    }

    @DELETE
    public void deleteAll() {
        this.store.clear();
    }
}