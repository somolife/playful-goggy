package com.threeclicks.scheduler.resources;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.threeclicks.scheduler.api.*;
import com.threeclicks.scheduler.api.cover.CompositeCover;
import com.threeclicks.scheduler.api.cover.CoverRequirement;
import com.threeclicks.scheduler.mapper.EmployeeSchedule;
import com.threeclicks.scheduler.mapper.RosterConverter;
import org.assertj.core.util.Lists;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.threeclicks.scheduler.api.DateRange.START_DATE_COMPARATOR;
import static com.threeclicks.scheduler.resources.ResourceUtils.notFoundWrapper;

@Path("/constraints")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConstraintsResource {

    private Map<Long, NurseRoster> rosterStore;
    private NavigableSet<Period> ordering;
    private Map<Long, Period> periodStore;

    @Inject
    private SchedulesResource schedulesResource;
    @Inject
    private EmployeesResource employeesResource;
    @Inject
    private ContractsResource contractsResource;
    @Inject
    @Named("aysncHandler")
    private Consumer<Callable> async;

    public ConstraintsResource() {
        this.rosterStore = Maps.newHashMap();
        this.periodStore = Maps.newHashMap();
        this.ordering = new TreeSet<>((p1, p2) -> START_DATE_COMPARATOR.compare(p1.dateRange, p2.dateRange));
    }

    @DELETE
    public void deleteAll() {
        periodStore.clear();
        ordering.clear();
        rosterStore.clear();
        //todo: should delete propagate to schedulesResource?
    }

    @GET
    public List<Period> getAll() {
        return Lists.newArrayList(this.ordering);
    }

    @GET
    @Path("/today")
    public Period getToday() throws IOException {
        Optional<Period> any = periodStore.values().stream()
                .filter(period -> period.containsDate(LocalDate.now()))
                .findAny();
        if (!any.isPresent()) {
            throw new NotFoundException();
        }
        return any.get();
    }

    @GET
    @Path("/{id}")
    public Period get(@PathParam("id") Long id) {
        return notFoundWrapper(periodStore.get(id));
    }

    @POST
    @Path("/{id}")
    public void post(@PathParam("id") final Long id, final Period period) throws IOException {
        period.setId(id);
        createEmployeesIfWeNeedMore(period);
        updateStore(id, period);
    }

    private void updateStore(Long id, Period period) throws IOException {
        period.setContracts(contractsResource.getAll().values());
        period.setEmployees(employees(period));

        if (period.getShiftCover() == null) {
            period.setDefaultSkillShiftCover(new ShiftCover(period.shiftCoverPerDay, Lists.emptyList(), Lists.emptyList()));
        }
        if (period.request == null) {
            period.setRequest(new Request(Maps.newHashMap()));
        }
        NurseRoster nurseRoster = new RosterConverter().convert(period);

        periodStore.put(id, period);
        ordering.add(period);
        rosterStore.put(id, nurseRoster);
    }

    private List<Employee> employees(Period period) throws IOException {
        return employeesResource.getAll().values()
                .stream()
                .limit(period.numEmployees)
                .collect(Collectors.toList());
    }

    private void postSchedules(Long id, Period period) throws IOException {
        this.schedulesResource.post(id, period);
    }

    private void createEmployeesIfWeNeedMore(Period period) throws IOException {
        int numWeHave = this.employeesResource.getAll().size();
        int numWeNeed = Math.max(0, period.numEmployees - numWeHave);
        if (numWeNeed > 0) {
            List<Employee> created = Employees.createEmployees(numWeNeed);
            for (Employee e : created) {
                this.employeesResource.post(e.getId(), e);
            }
        }
    }

    @GET
    @Path("/{id}/schedule")
    public EmployeeSchedule getSchedules(@PathParam("id") Long id) throws IOException {
        return EmployeeSchedule.from(
                notFoundWrapper(rosterStore.get(id)),
                employeesResource.getAll());
    }

    @GET
    @Path("/{id}/requests")
    public Request getRequests(@PathParam("id") Long id) throws IOException {
        return get(id).request;
    }

    @POST
    @Path("/{id}/requests")
    public void postRequests(@PathParam("id") Long id, final Request requests) throws IOException {
        Period period = get(id);
        period.setRequest(requests);
        this.updateStore(id, period);
        this.async.accept(() -> {
            postSchedules(id, period);
            return null;
        });
    }

    @GET
    @Path("/{id}/shiftCover")
    public CoverRequirement getShiftCover(@PathParam("id") Long id) throws IOException {
        return get(id).getDefaultShiftCover();
    }

    @POST
    @Path("/{id}/shiftCover")
    public void postShiftCover(@PathParam("id") Long id, final ShiftCover shiftCover) throws IOException {
        Period period = get(id);
        period.setDefaultSkillShiftCover(shiftCover);
        this.updateStore(id, period);
    }

    @GET
    @Path("/{id}/dimensionCover")
    public CompositeCover getDimensionCover(@PathParam("id") Long id) throws IOException {
        return get(id).getShiftCover();
    }

    @POST
    @Path("/{id}/dimensionCover")
    public void postDimensionCover(@PathParam("id") Long id, final CompositeCover compositeCover) throws IOException {
        Period period = get(id);
        period.setShiftCover(compositeCover);
        this.updateStore(id, period);
    }

    //todo: clone next/prev should carry dimensionCover requirement

    @GET
    @Path("/{id}/next")
    public Period getNext(@PathParam("id") Long id) throws IOException {
        return notFoundWrapper(ordering.higher(get(id)));
    }

    @POST
    @Path("/{id}/next")
    public void postNext(@PathParam("id") Long id) throws IOException {
        post(id + 1, get(id).next());
    }

    @GET
    @Path("/{id}/prev")
    public Period getPrevious(@PathParam("id") Long id) throws IOException {
        return notFoundWrapper(ordering.lower(get(id)));
    }

    @POST
    @Path("/{id}/prev")
    public void postPrevious(@PathParam("id") Long id) throws IOException {
        post(id - 1, get(id).previous());
    }

}