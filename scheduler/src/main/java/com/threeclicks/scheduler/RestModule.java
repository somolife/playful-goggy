package com.threeclicks.scheduler;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import com.threeclicks.scheduler.resources.ConstraintsResource;
import com.threeclicks.scheduler.resources.ContractsResource;
import com.threeclicks.scheduler.resources.EmployeesResource;
import com.threeclicks.scheduler.resources.SchedulesResource;
import com.threeclicks.scheduler.utils.AsyncHandler;

import java.util.concurrent.Callable;
import java.util.function.Consumer;

/**
 * @author nigeldev, @date 2/28/18
 */
public class RestModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(new TypeLiteral<Consumer<Callable>>() {})
                .annotatedWith(Names.named("aysncHandler"))
                .toInstance(new AsyncHandler());

        bind(ContractsResource.class).toInstance(new ContractsResource());
        bind(EmployeesResource.class).toInstance(new EmployeesResource());
        bind(SchedulesResource.class).toInstance(new SchedulesResource());
        bind(ConstraintsResource.class).toInstance(new ConstraintsResource());
    }
}
