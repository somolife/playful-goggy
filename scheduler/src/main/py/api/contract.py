import json
from api import contract_api


def _weighted_value(value, weight):
    return {'value': value, 'weight': weight}


class ContractBuilder():
    def __init__(self, code):
        self.data = {'code': code, 'weekendDefnCode': 'SaturdaySunday'}

    def booleanConstraint(self, name, value, weight):
        self.data[name] = _weighted_value(value, weight)
        return self

    def minMaxConstraint(self, name, min, max):
        self.data[name] = {'min': min, 'max': max}
        return self

    def build(self):
        return self.data


def get_a_contract():
    return ContractBuilder('FT') \
        .booleanConstraint('singleAssignmentPerDay', True, 50) \
        .booleanConstraint('identicalShiftTypesDuringWeekend', True, 4) \
        .minMaxConstraint('totalAssignments', _weighted_value(18, 20), _weighted_value(18, 30)) \
        .minMaxConstraint('consecutiveWorkingDays', _weighted_value(2, 15), _weighted_value(3, 15)) \
        .minMaxConstraint('consecutiveWorkingWeekends', _weighted_value(1, 2), _weighted_value(2, 3)) \
        .minMaxConstraint('consecutiveFreeDays', _weighted_value(2, 5), _weighted_value(2, 6)) \
        .build()


print json.dumps(contract_api.get())
# contract = get_a_contract()
# print json.dumps(contract)
# contract_api.post(contract['code'], contract)