import requests


class Api:
    def __init__(self, end_pt):
        self.url = 'http://localhost:9000/' + end_pt
        pass

    def get(self):
        r = requests.get(self.url)
        # todo: error check/handling
        print r
        return r.json()

    def post(self, _id, _data):
        url = self.url + '/' + _id
        r = requests.post(url, json=_data)
        print r
        return r

    def delete(self):
        pass


contract_api = Api('contracts')
