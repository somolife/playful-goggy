package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.threeclicks.scheduler.api.cover.CompositeCover;
import com.threeclicks.scheduler.api.cover.DimensionRequirement;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.io.IOException;
import java.time.DayOfWeek;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author nigeldev, @date 3/15/18
 */
public class ShiftCoverTest {
    private static final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void checkMondayVsMonday() {
        DayOfWeek monday = DayOfWeek.MONDAY;
        org.optaplanner.examples.nurserostering.domain.DayOfWeek otherMonday =
                org.optaplanner.examples.nurserostering.domain.DayOfWeek.MONDAY;
        assertEquals(monday.getValue() - 1, otherMonday.ordinal());
    }

    @Test
    public void checkCreation() {
        assertEquals(100, new ShiftCover(100, Lists.emptyList(), Lists.emptyList())
                .getCover(1, ""));

        ShiftCover r2 = new ShiftCover(100,
                Lists.newArrayList(new ShiftCover.DayOfWeekCover(1, 5)),
                Lists.emptyList());
        assertEquals(2, r2.numCovers());
    }

    @Test
    public void checkWeekdayRequest() {
        ShiftCover r = new ShiftCover(100,
                Lists.newArrayList(
                        new ShiftCover.DayOfWeekCover(DayOfWeek.MONDAY.getValue(), 5),
                        new ShiftCover.DayOfWeekCover(DayOfWeek.THURSDAY.getValue(), 10),
                        new ShiftCover.DayOfWeekCover(DayOfWeek.FRIDAY.getValue(), 12)
                ),
                Lists.emptyList());
        assertEquals(5, r.getCover(1, ""));
        assertEquals(10, r.getCover(4, ""));
        assertEquals(12, r.getCover(5, ""));

    }

    @Test
    public void checkDateRequest() {
        ShiftCover r = new ShiftCover(100,
                Lists.emptyList(),
                Lists.newArrayList(
                        new ShiftCover.SpecficDateCover("day1", 5),
                        new ShiftCover.SpecficDateCover("day4", 10),
                        new ShiftCover.SpecficDateCover("day5", 12)
                ));
        assertEquals(5, r.getCover(0, "day1"));
        assertEquals(10, r.getCover(0, "day4"));
        assertEquals(12, r.getCover(0, "day5"));
        assertEquals(100, r.getCover(0, "day10"));
    }

    @Test
    public void checkMixedRequest() {
        ShiftCover r = new ShiftCover(100,
                Lists.newArrayList(
                        new ShiftCover.DayOfWeekCover(1, 1),
                        new ShiftCover.DayOfWeekCover(3, 3),
                        new ShiftCover.DayOfWeekCover(6, 6)
                ),
                Lists.newArrayList(
                        new ShiftCover.SpecficDateCover("day1", 10),
                        new ShiftCover.SpecficDateCover("day4", 40),
                        new ShiftCover.SpecficDateCover("day5", 50)
                ));

        assertEquals(10, r.getCover(1, "day1"));
        assertEquals(3, r.getCover(3, "day3"));
        assertEquals(50, r.getCover(5, "day5"));
    }

    @Test
    public void checkSomeSerializationDude() throws JsonProcessingException {

        ShiftCover r = new ShiftCover(10,
                Lists.newArrayList(
                        new ShiftCover.DayOfWeekCover(3, 8)
                ),
                Lists.newArrayList(
                        new ShiftCover.SpecficDateCover("day1", 12),
                        new ShiftCover.SpecficDateCover("day2", 14)
                ));
        final String s = mapper.writeValueAsString(r);
        assertNotNull(s);
    }

    @Test
    public void givenASetOfSkills_customizeCover() throws IOException {

        CompositeKey nurse = CompositeKey.forSkill("nurse");
        DimensionRequirement nurseReq = new DimensionRequirement(new ShiftCover(10), nurse);
        assertEquals(10, nurseReq.getCover(1, ""));

        CompositeKey manager = CompositeKey.forSkill("manager");
        DimensionRequirement managerReq = new DimensionRequirement(new ShiftCover(20), manager);
        assertEquals(20, managerReq.getCover(1, ""));

        // Given a set of cover requirements, I should be able to pick the cover for a particular code
        CompositeCover cover = CompositeCover.create(nurseReq, managerReq);
        assertEquals(10, cover.get(nurse).getCover(1, ""));
    }

    @Test
    public void givenSkillsAndLocations_customizeCover() throws IOException {

        //submit the requirement
        CompositeKey d1 = CompositeKey.forLocationAndSkill("nyc", "nurse");
        DimensionRequirement req1 = new DimensionRequirement(
                new ShiftCover(12,
                        Lists.newArrayList(new ShiftCover.DayOfWeekCover(1, 10)),
                        Lists.newArrayList()),
                d1);

        CompositeKey d2 = CompositeKey.forLocationAndSkill("nyc", "manager");
        DimensionRequirement req2 = new DimensionRequirement(new ShiftCover(20), d2);

        CompositeCover cover = CompositeCover.create(req2, req1);
        assertEquals(10, cover.get(d1).getCover(1, ""));
        assertEquals(20, cover.get(d2).getCover(1, ""));

        final String dimStr = mapper.writeValueAsString(d1);
        assertNotNull(dimStr);
        final CompositeKey readDim = mapper.readValue(dimStr, CompositeKey.class);
        assertNotNull(readDim);

        final String reqStr = mapper.writeValueAsString(req1);
        assertNotNull(reqStr);
        final DimensionRequirement readReq = mapper.readValue(reqStr, DimensionRequirement.class);
        assertNotNull(readReq);

        final String coverStr = mapper.writeValueAsString(cover);
        assertNotNull(coverStr);
        final CompositeCover readCov = mapper.readValue(coverStr, CompositeCover.class);
        assertNotNull(readCov);
    }
}
