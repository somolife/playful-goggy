package com.threeclicks.scheduler.api;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author nigeldev, @date 2/15/18 9:51 AM
 */
public class EmployeesTest {
    @Test
    public void createOneEmployee() {
        assertEquals(1, Employees.createEmployees(1).size());
    }

    @Test
    public void createMultipleEmployees() {
        assertEquals(2, Employees.createEmployees(2).size());
    }

    @Test
    public void testAssignContract() {
        List<Employee> employees = Employees.createEmployees(3);
        String contractCode = employees.get(0).getContractCode();
        Assert.assertEquals(Contract.DEFAULT_CODE, contractCode);
    }
}
