package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.threeclicks.scheduler.mapper.RosterConverter;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.solver.SolverBuilder;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * mostly concerned about serialization
 *
 * @author nigeldev, @date 2/15/18 9:51 AM
 */
public class EmployeeTest {

    @Test
    public void checkNewObjToAndFromJson() throws IOException {
        Employee e = Employees.createEmployeesFromScratch(1).get(0);
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(e);
        assertNotNull(value);
        Employee eFromStr = mapper.readValue(value, Employee.class);
        assertNotNull(eFromStr);
        assertEquals(e, eFromStr);
        assertEquals(e.getId(), eFromStr.getId());
        assertEquals(e.getSeniority(), eFromStr.getSeniority());
    }

    @Test
    public void checkContractJson() throws IOException {
        Employee e = Employees.createEmployeesFromScratch(1).get(0);
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(e);
        assertNotNull(value);
        Employee eFromStr = mapper.readValue(value, Employee.class);
        String code = eFromStr.getContractCode();
        assertEquals(Contract.DEFAULT_CODE, code);
    }

    @Test
    public void testSenioritySettings() throws IOException {
        List<Employee> employees = Employees.createEmployeesFromScratch(2);
        employees.get(0).setSeniority(100);
        int numDays = 1;
        String day1 = "2018-02-02";
        Period basic = new Period.Builder()
                .numDays(numDays)
                .startDate(day1)
                .shiftCoverPerDay(1)
                .employees(employees)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .daysOff(0, Lists.newArrayList(day1))
                .daysOff(1, Lists.newArrayList(day1))
                .build();
        NurseRoster roster = new RosterConverter().convert(basic);
        Assert.assertEquals(190, roster.getDayOffRequestList().get(0).getWeight());
        Assert.assertEquals(91, roster.getDayOffRequestList().get(1).getWeight());
    }

    @Test
    @Ignore
    public void testSeniorityUsingSolverInSandbox() throws IOException {
        List<Employee> employees = Employees.createEmployeesFromScratch(5);
        Period basic = new Period.Builder()
                .numDays(14)
                .startDate("2018-02-02")
                .shiftCoverPerDay(4)
                .employees(employees)
                .daysOff(0, Lists.newArrayList("2018-02-04"))
                .daysOff(1, Lists.newArrayList("2018-02-05", "2018-02-06"))
                .daysOff(2, Lists.newArrayList("2018-02-02", "2018-02-03"))
                .daysOff(3, Lists.newArrayList("2018-02-04"))
                .daysOff(4, Lists.newArrayList("2018-02-06", "2018-02-07"))
                .build();
        NurseRoster roster = new RosterConverter().convert(basic);

        Solver<Solution> solver = new SolverBuilder().timeLimit(1).build();
        NurseRoster solution = (NurseRoster) solver.solve(roster);

        Assert.assertNotNull(solution);
    }

}
