package com.threeclicks.scheduler.api;

import org.assertj.core.util.Lists;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.NavigableSet;
import java.util.TreeSet;

import static org.junit.Assert.*;

/**
 * @author nigeldev, @date 3/9/18
 */
public class DateRangeTest {
    @Test
    public void testIt() {
        int numDays = 7;
        DateRange dr = new DateRange("2018-01-01", numDays);
        LocalDate day1 = dr.stream().findFirst().get();
        assertEquals("2018-01-01", DateRange.formatter.format(day1));
        assertEquals(numDays, dr.stream().count());
        assertEquals("2018-01-07", dr.endDate.format(DateRange.formatter));
    }

    @Test
    public void testPrevious() {
        DateRange dr = new DateRange("2018-03-05", 7);
        DateRange previous = dr.previous();
        assertEquals("2018-02-26", previous.startDateStr);
        assertEquals("2018-03-04", previous.endDate.format(DateRange.formatter));
    }

    @Test
    public void testNext() {
        DateRange dr = new DateRange("2018-03-05", 7);
        DateRange next = dr.next();
        assertEquals("2018-03-12", next.startDateStr);
        assertEquals("2018-03-18", next.endDate.format(DateRange.formatter));
    }

    @Test
    public void testContains() {
        DateRange dr = new DateRange("2018-03-05", 7);
        assertFalse(dr.contains(LocalDate.parse("2018-03-04")));
        assertTrue(dr.contains(LocalDate.parse("2018-03-05")));
        assertTrue(dr.contains(LocalDate.parse("2018-03-08")));
        assertTrue(dr.contains(LocalDate.parse("2018-03-11")));
        assertFalse(dr.contains(LocalDate.parse("2018-03-12")));
    }

    @Test
    public void testOrderingInASet() {
        DateRange dr1 = new DateRange("2018-03-05", 7);
        DateRange dr2 = dr1.next();
        DateRange dr0 = dr1.previous();
        NavigableSet<DateRange> set = new TreeSet<>(DateRange.START_DATE_COMPARATOR);
        set.add(dr2);
        set.add(dr0);
        set.add(dr1);
        assertEquals(dr1, set.higher(dr0));
        assertEquals(dr1, set.lower(dr2));
        assertNull(set.lower(dr0));
    }

    @Test
    public void testOrderingWhenAddedToAList(){
        DateRange dr1 = new DateRange("2018-03-05", 7);
        DateRange dr2 = dr1.next();
        DateRange dr0 = dr1.previous();
        NavigableSet<DateRange> set = new TreeSet<>(DateRange.START_DATE_COMPARATOR);
        set.add(dr2);
        set.add(dr0);
        set.add(dr1);
        ArrayList<DateRange> dateRanges = Lists.newArrayList(set);
        assertEquals(dr0, dateRanges.get(0));
        assertEquals(dr1, dateRanges.get(1));
        assertEquals(dr2, dateRanges.get(2));
    }
}
