package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author nigeldev, @date 2/15/18 9:51 AM
 */
public class RequestTest {
    @Test
    public void toAndFromJson() throws IOException {
        Request r = new Request();
        r.add(Request.Type.DAY_OFF, 100L, Lists.newArrayList("Mon"));
        r.add(Request.Type.DAY_ON, 100L, Lists.newArrayList("Tues"));
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(r);
        assertNotNull(value);
        Request rFromStr = mapper.readValue(value, Request.class);
        assertNotNull(rFromStr);
        assertEquals("Mon", rFromStr.get(Request.Type.DAY_OFF, 100L).get(0));
        assertEquals("Tues", rFromStr.get(Request.Type.DAY_ON, 100L).get(0));
    }
}
