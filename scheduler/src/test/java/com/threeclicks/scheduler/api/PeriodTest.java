package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author nigeldev, @date 2/15/18
 */
public class PeriodTest {

    @Test
    public void createOneWeeklyPeriod() {
        assertEquals(7, new Period.Builder().forOneWeek().build().numDays);
    }

    @Test
    public void createOneMonthlyPeriod() {
        assertEquals(28, new Period.Builder().forOneMonth().build().numDays);
    }

    @Test
    public void checkPeriodStartAndEnd() {
        Period p = new Period.Builder().forOneWeek().startDate("2018-02-12").build();
        assertEquals(LocalDate.parse("2018-02-12"), p.dateRange.startDate);
        assertEquals(LocalDate.parse("2018-02-18"), p.dateRange.endDate);
    }

    @Test
    public void checkHolidayCover() {
        Period p = new Period.Builder().
                forOneWeek().
                holidayCover(3, "2018-02-12", "2018-02-14").build();
        assertEquals(3, p.getDefaultShiftCover().numCovers());
    }

    @Test
    public void checkDayOfWeekCover() {
        Period p = new Period.Builder().
                forOneWeek()
                .dayOfWeekCover(5, DayOfWeek.WEDNESDAY.getValue(), DayOfWeek.THURSDAY.getValue(), DayOfWeek.FRIDAY.getValue())
                .build();
        assertEquals(4, p.getDefaultShiftCover().numCovers());
    }

    @Test
    public void canAddSomeEmployees() {
        Period p = new Period.Builder()
                .forOneWeek()
                .employees(Employees.createEmployees(3))
                .build();
        assertEquals(3, p.employees.size());
    }

    @Test
    public void canAddDayOffRequests() {
        List<Employee> employees = Employees.createEmployees(3);
        Period p = new Period.Builder()
                .forOneWeek()
                .employees(employees)
                .daysOff(0, Lists.newArrayList("2018-02-12"))
                .daysOff(2, Lists.newArrayList("2018-02-12", "2018-02-13"))
                .build();

        assertEquals(1, p.request.get(Request.Type.DAY_OFF, employees.get(0).getId()).size());
        assertEquals(2, p.request.get(Request.Type.DAY_OFF, employees.get(2).getId()).size());
    }

    @Test
    public void canAddVacationRequests() {
        List<Employee> employees = Employees.createEmployees(2);
        Period p = new Period.Builder()
                .forOneWeek()
                .employees(employees)
                .vacations(0, Lists.newArrayList("2018-02-14", "2018-02-15"))
                .build();
        assertEquals(2, p.request.get(Request.Type.VACATION, employees.get(0).getId()).size());
        assertNull(p.request.get(Request.Type.VACATION, employees.get(1).getId()));
    }

    @Test
    public void canAddDayOnRequests() {
        List<Employee> employees = Employees.createEmployees(2);
        Period p = new Period.Builder()
                .forOneWeek()
                .employees(employees)
                .daysOn(0, Lists.newArrayList("2018-02-14", "2018-02-15"))
                .build();
        assertEquals(2, p.request.get(Request.Type.DAY_ON, employees.get(0).getId()).size());
        assertNull(p.request.get(Request.Type.DAY_ON, employees.get(1).getId()));
    }

    @Test
    public void toAndFromJson() throws IOException {
        List<Employee> employees = Employees.createEmployees(2);
        Period p = new Period.Builder()
                .forOneWeek()
                .startDate("2018-02-10")
                .employees(employees)
                .daysOff(0, Lists.newArrayList("2018-02-12"))
                .daysOff(1, Lists.newArrayList("2018-02-12", "2018-02-13"))
                .build();
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(p);
        assertNotNull(value);

        Period period = mapper.readValue(value, Period.class);
        assertEquals(7, period.numDays);
        assertEquals(2, period.numEmployees);
        assertEquals(1, period.shiftCoverPerDay);
        assertEquals("2018-02-10", period.getStartDateStr());

        //confirm these are NOT serialized
        assertNull(period.request);
        assertNull(period.employees);
    }

    @Test
    public void sampleJson() throws JsonProcessingException {
        List<Employee> employees = Employees.createEmployees(5);
        Period basic = new Period.Builder()
                .numDays(7)
                .startDate("2018-02-02")
                .shiftCoverPerDay(4)
                .employees(employees)
                .daysOff(0, Lists.newArrayList("2018-02-04"))
                .daysOff(1, Lists.newArrayList("2018-02-02", "2018-02-05", "2018-02-06"))
                .daysOff(2, Lists.newArrayList("2018-02-02", "2018-02-03"))
                .daysOff(4, Lists.newArrayList("2018-02-05", "2018-02-07"))
                .build();
        ObjectMapper mapper = new ObjectMapper();
        String value = mapper.writeValueAsString(basic);
        assertNotNull(value);
    }
}
