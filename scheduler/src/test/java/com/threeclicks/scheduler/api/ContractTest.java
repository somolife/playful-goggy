package com.threeclicks.scheduler.api;

import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.WeekendDefinition;

import static org.junit.Assert.assertEquals;

/**
 * @author nigeldev, @date 2/16/18
 */
public class ContractTest {

    @Test
    public void defaultContract() {
        Contract c = Contract.defaultContract();
        //todo: test weights
        assertEquals(30, c.totalAssignments.max.weight);
        assertEquals(20, c.totalAssignments.min.weight);
        assertEquals(18, c.totalAssignments.max.value.intValue());
        assertEquals(18, c.totalAssignments.min.value.intValue());
        assertEquals(3, c.consecutiveWorkingDays.max.value.intValue());
        assertEquals(2, c.consecutiveFreeDays.max.value.intValue());
        assertEquals(1, c.consecutiveWorkingWeekends.min.value.intValue());
        WeekendDefinition weekendDefinition = WeekendDefinition.valueOfCode(c.weekendDefnCode);
        assertEquals(WeekendDefinition.SATURDAY_SUNDAY, weekendDefinition);
        assertEquals(2, weekendDefinition.getWeekendLength());
        assertEquals(60, c.maxWeight());
    }
}
