package com.threeclicks.scheduler.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author nigeldev, @date 3/29/18
 */
public class CompositeKeyTest {
    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void checkThisKeyOut() throws JsonProcessingException {
        CompositeKey key = CompositeKey.forLocationAndSkill("NYC", "MD");

        assertFalse(key.partialMatch("{\"SKILL\":\"NP\"}"));
        assertTrue(key.partialMatch("{\"SKILL\":\"MD\"}"));

        //of course I should be able to match the entire key
        assertTrue(key.partialMatch(mapper.writeValueAsString(ImmutableMap.of(
                CompositeKey.Type.SKILL.toString(), "MD",
                CompositeKey.Type.LOCATION.toString(), "NYC"))));

        assertFalse(key.partialMatch(mapper.writeValueAsString(ImmutableMap.of(
                CompositeKey.Type.SKILL.toString(), "NP",
                CompositeKey.Type.LOCATION.toString(), "NYC"))));
    }
}
