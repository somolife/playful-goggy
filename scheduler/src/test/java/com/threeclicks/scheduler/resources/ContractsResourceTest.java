package com.threeclicks.scheduler.resources;

import com.threeclicks.scheduler.api.Contract;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * @author nigeldev, @date 3/1/18
 */
public class ContractsResourceTest {

    private static ContractsResource contractsRes = new ContractsResource();

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(contractsRes)
            .build();

    @Test
    public void testWhenConstructed() {
        Map map = resources.client()
                .target("/contracts")
                .request().get(new GenericType<Map<String, Contract>>() {
                });
        assertEquals(1, map.values().size());
        assertEquals(Contract.defaultContract(), map.values().stream().findFirst().get());
    }

    @Test(expected = ProcessingException.class)
    public void testPostSingleContract() {
        Contract mock = mock(Contract.class);
        resources.client()
                .target("/contracts")
                .request().post(Entity.entity(mock, MediaType.APPLICATION_JSON_TYPE));
    }

    @Test(expected = NotFoundException.class)
    public void get_MissingContract() {
        resources.client()
                .target("/contracts/MISSING")
                .request().get(Contract.class);
    }
}
