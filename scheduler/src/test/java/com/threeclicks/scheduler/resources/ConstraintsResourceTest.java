package com.threeclicks.scheduler.resources;

import com.google.common.collect.ImmutableMap;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import com.threeclicks.scheduler.api.*;
import com.threeclicks.scheduler.api.cover.CompositeCover;
import com.threeclicks.scheduler.api.cover.CoverRequirement;
import com.threeclicks.scheduler.api.cover.DimensionRequirement;
import com.threeclicks.scheduler.mapper.EmployeeSchedule;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.time.DayOfWeek;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

import static com.threeclicks.scheduler.api.CompositeKey.Type.LOCATION;
import static com.threeclicks.scheduler.api.CompositeKey.Type.SKILL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * http://www.dropwizard.io/0.8.2/docs/manual/testing.html
 *
 * @author nigeldev, @date 2/15/18 9:51 AM
 */
public class ConstraintsResourceTest {

    static final String START_DATE = "2018-02-02";
    static final String START_DATE_PLUS1 = "2018-02-03";
    static final String START_DATE_PLUS5 = "2018-02-07";

    private static Consumer<Callable> consumer = c -> {
        try {
            c.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    };

    private static Injector injector = Guice.createInjector(new AbstractModule() {
        @Override
        protected void configure() {
            bind(new TypeLiteral<Consumer<Callable>>() {
            })
                    .annotatedWith(Names.named("aysncHandler"))
                    .toInstance(consumer);
            bind(ContractsResource.class).toInstance(mock(ContractsResource.class));
            bind(EmployeesResource.class).toInstance(mock(EmployeesResource.class));
            bind(SchedulesResource.class).toInstance(mock(SchedulesResource.class));
        }
    });

    private static EmployeesResource employeesRes = injector.getInstance(EmployeesResource.class);
    private static ContractsResource contractsRes = injector.getInstance(ContractsResource.class);
    private static SchedulesResource schedulesRes = injector.getInstance(SchedulesResource.class);
    private static ConstraintsResource constraintRes = injector.getInstance(ConstraintsResource.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(constraintRes)
            .addResource(schedulesRes)
            .addResource(employeesRes)
            .addResource(contractsRes)
            .build();

    @Before
    public void setup() {
        reset(employeesRes);
        reset(contractsRes);
        reset(schedulesRes);
    }

    @Test(expected = NotFoundException.class)
    public void excpWhen_constraintMissing() throws IOException {
        resources.client().target("/constraints/1/schedule").request().get(EmployeeSchedule.class);
    }

    @Test(expected = ProcessingException.class)
    public void excpWhen_postEmptyPeriod() {
        Period period = new Period.Builder().build();
        Response response = resources.client().target("/constraints/1")
                .request().post(Entity.entity(period, MediaType.APPLICATION_JSON_TYPE));
        assertThat(response).isNotNull();
    }

    @Test
    public void okWhen_postInconsistentPeriodId() {
        Period period = new Period.Builder().numDays(1).startDate("2017-01-01").build();
        Response response = resources.client().target("/constraints/2")
                .request().post(Entity.entity(period, MediaType.APPLICATION_JSON_TYPE));
        assertThat(response).isNotNull();
        Period retrieved = resources.client().target("/constraints/2")
                .request().get(Period.class);
        assertThat(retrieved.id).isNotEqualTo(period.id);
    }

    @Test
    public void postBasicPeriod() throws IOException {
        mockDefaultContract();
        mockSingleEmployee();

        Period period = getPeriod();
        Response r1 = resources.client().target("/constraints/1")
                .request().post(Entity.entity(period, MediaType.APPLICATION_JSON_TYPE));
        assertThat(r1.getStatusInfo().getFamily()).isEqualTo(Response.Status.Family.SUCCESSFUL);

        verify(contractsRes, times(1)).getAll();
        verify(employeesRes, times(2)).getAll();
        assertNoMoreMockInteractions();

        Period retrieved = resources.client().target("/constraints/1")
                .request().get(Period.class);
        assertThat(retrieved).isEqualTo(period);
        EmployeeSchedule schedule = resources.client().target("/constraints/1/schedule")
                .request().get(EmployeeSchedule.class);
        assertThat(schedule).isNotNull();

        ShiftCover shiftCover = resources.client().target("/constraints/1/shiftCover")
                .request().get(ShiftCover.class);
        assertThat(shiftCover.numCovers()).isEqualTo(1);
        assertEquals(period.shiftCoverPerDay, shiftCover.getCover(0, ""));
    }

    private void assertNoMoreMockInteractions() {
        verifyNoMoreInteractions(employeesRes);
        verifyNoMoreInteractions(contractsRes);
        verifyNoMoreInteractions(schedulesRes);
    }

    private Period getPeriod() {
        return getPeriodBuilder().build();
    }

    private Period.Builder getPeriodBuilder() {
        return new Period.Builder()
                .numDays(5)
                .startDate(START_DATE)
                .shiftCoverPerDay(1)
                .numEmployees(1);
    }

    private void mockSingleEmployee() throws IOException {
        final List<Employee> employees = Employees.createEmployees(1);
        Employee employee = employees.get(0);
        when(employeesRes.getAll()).thenReturn(ImmutableMap.of(employee.getId(), employee));
    }

    private void mockDefaultContract() throws IOException {
        final Contract contract = Contract.defaultContract();
        when(contractsRes.getAll()).thenReturn(ImmutableMap.of(contract.code, contract));
    }

    @Test
    public void postBasicPeriodWithNoExistingEmployees() throws IOException {
        Response r1 = resources.client().target("/constraints/1")
                .request().post(Entity.entity(getPeriod(), MediaType.APPLICATION_JSON_TYPE));
        assertThat(r1.getStatusInfo().getFamily()).isEqualTo(Response.Status.Family.SUCCESSFUL);

        verify(contractsRes, times(1)).getAll();
        verify(employeesRes, times(2)).getAll();
        verify(employeesRes, times(1)).post(any(Long.class), any(Employee.class));

        assertNoMoreMockInteractions();
    }

    @Test
    public void postPeriodFollowedByRequestWithOneEmployee() throws IOException {
        this.mockSingleEmployee();
        this.mockDefaultContract();

        Employee employee = employeesRes.getAll().entrySet().stream().findFirst().get().getValue();

        resources.client().target("/constraints/1")
                .request().post(Entity.entity(getPeriod(), MediaType.APPLICATION_JSON_TYPE));

        Request request = new Request(
                ImmutableMap.of(Request.Type.DAY_OFF,
                        ImmutableMap.of(employee.getId(),
                                Lists.newArrayList("2018-02-02"))));

        Response r2 = resources.client().target("/constraints/1/requests")
                .request().post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE));
        assertThat(r2.getStatusInfo().getFamily()).isEqualTo(Response.Status.Family.SUCCESSFUL);

        verify(schedulesRes, times(1)).post(eq(1L), any(Period.class));
        verifyNoMoreInteractions(schedulesRes);
    }

    @Test
    public void postNextPeriod() throws IOException {
        mockDefaultContract();
        mockSingleEmployee();

        resources.client().target("/constraints/1")
                .request().post(Entity.entity(getPeriod(), MediaType.APPLICATION_JSON_TYPE));

        Response r2 = resources.client().target("/constraints/1/next")
                .request().post(null);
        assertThat(r2.getStatusInfo().getFamily()).isEqualTo(Response.Status.Family.SUCCESSFUL);

        Period retrieved = resources.client().target("/constraints/2")
                .request().get(Period.class);
        assertThat(retrieved).isNotNull();
        assertThat(retrieved.dateRange.startDateStr).isEqualTo(START_DATE_PLUS5);

        Period retrieved2 = resources.client().target("/constraints/1/next")
                .request().get(Period.class);
        assertThat(retrieved2).isNotNull();
        assertThat(retrieved2.dateRange.startDateStr).isEqualTo(START_DATE_PLUS5);

        Period retrieved3 = resources.client().target("/constraints/2/prev")
                .request().get(Period.class);
        assertThat(retrieved3).isNotNull();
        assertThat(retrieved3.dateRange.startDateStr).isEqualTo(START_DATE);
    }

    @Test
    public void postSomeShiftCovers() throws IOException {
        mockDefaultContract();
        mockSingleEmployee();

        final String uri = "/constraints/1/";
        final Period period = getPeriod();

        resources.client().target(uri)
                .request().post(Entity.entity(period, MediaType.APPLICATION_JSON_TYPE));

        final ShiftCover coverRequest = new ShiftCover(period.shiftCoverPerDay,
                Lists.newArrayList(new ShiftCover.DayOfWeekCover(DayOfWeek.MONDAY.getValue(), 2)),
                Lists.emptyList());

        final String uriCovers = uri + "shiftCover";
        Response r2 = resources.client().target(uriCovers)
                .request().post(Entity.entity(coverRequest, MediaType.APPLICATION_JSON_TYPE));
        assertThat(r2.getStatusInfo().getFamily()).isEqualTo(Response.Status.Family.SUCCESSFUL);

        final CoverRequirement retrieved = resources.client().target(uriCovers)
                .request().get(ShiftCover.class);
        assertThat(retrieved.numCovers()).isEqualTo(coverRequest.numCovers());
    }

    @Test
    public void postSomeSkillsShiftCovers() throws IOException {
        mockDefaultContract();
        mockSingleEmployee();

        final String uri1 = "/constraints/1/";
        final Period period = getPeriod();

        resources.client().target(uri1)
                .request().post(Entity.entity(period, MediaType.APPLICATION_JSON_TYPE));

        DimensionRequirement req1 = new DimensionRequirement(new ShiftCover(12),
                CompositeKey.forLocationAndSkill("nyc", "nurse"));

        DimensionRequirement req2 = new DimensionRequirement(new ShiftCover(20),
                CompositeKey.forLocationAndSkill("nyc", "manager"));

        CompositeCover cover = CompositeCover.create(req1, req2);

        final String uriCovers = uri1 + "dimensionCover";
        Response r2 = resources.client().target(uriCovers)
                .request().post(Entity.entity(cover, MediaType.APPLICATION_JSON_TYPE));
        assertThat(r2.getStatusInfo().getFamily()).isEqualTo(Response.Status.Family.SUCCESSFUL);

        final CompositeCover retrieved = resources.client().target(uriCovers)
                .request().get(CompositeCover.class);
        assertThat(retrieved.get(req2.dimension).getCover(1, "")).isEqualTo(20);

        resources.client().target(uri1 + "next")
                .request().post(null);
        final String uri2 = "/constraints/2/";
        CompositeCover retrieved2 = resources.client().target(uri2 + "dimensionCover").request().get(CompositeCover.class);
        assertThat(retrieved2).isNotNull();
        assertThat(retrieved2.map.keySet()).isEqualTo(cover.map.keySet());
    }

}