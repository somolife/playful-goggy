package com.threeclicks.scheduler.mapper.rule;

import com.google.common.collect.Lists;
import com.threeclicks.scheduler.api.Employee;
import com.threeclicks.scheduler.api.Employees;
import com.threeclicks.scheduler.mapper.EmployeeConverter;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.domain.ShiftAssignment;

import java.util.List;
import java.util.stream.Collectors;

import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getNurseRoster;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author nigeldev, @date 3/27/18
 */
public class MinShiftPerEmployeeTest {

    @Test
    public void employeeMinShiftRequirement_wasMet() {
        List<Employee> employees = Employees.createEmployees(1);
        NurseRoster nurseRoster = getNurseRoster(employees, "2018-02-02", 2);
        ShiftAssignment shiftAssignment = new ShiftAssignment();
//        ContractConverter contracts = new ContractConverter(period);
        shiftAssignment.setEmployee(EmployeeConverter.getEmployee(employees.get(0)));
        nurseRoster.setShiftAssignmentList(Lists.newArrayList(shiftAssignment));
        final List list = new MinShiftPerEmployee().apply(nurseRoster);
        assertTrue(list.isEmpty());
    }

    @Test
    public void employeeMinShiftRequirement_notMet() {
        List<Employee> employees = Employees.createEmployees(1);
        NurseRoster nurseRoster = getNurseRoster(employees, "2018-02-02", 2);
        nurseRoster.setShiftAssignmentList(Lists.newArrayList(new ShiftAssignment()));
        final List list = new MinShiftPerEmployee().apply(nurseRoster);
        assertFalse(list.isEmpty());
        assertTrue(list.equals(employees.stream().map(Employee::getId).collect(Collectors.toList())));
    }
}
