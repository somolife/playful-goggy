package com.threeclicks.scheduler.mapper.utils;

import com.google.common.collect.Lists;
import com.threeclicks.scheduler.api.Contract;
import com.threeclicks.scheduler.api.Employee;
import com.threeclicks.scheduler.api.Period;
import com.threeclicks.scheduler.mapper.EmployeeConverter;
import com.threeclicks.scheduler.mapper.RosterConverter;
import org.optaplanner.examples.nurserostering.domain.*;
import org.optaplanner.examples.nurserostering.domain.request.DayOffRequest;

import java.util.List;

/**
 * @author nigeldev, @date 3/26/18
 */
public class NurseRosterUtils {

    public static ShiftAssignment getShiftAssignment(Employee employee, int dayIndex, String shiftTypeCode) {
        ShiftAssignment shiftAssignment = new ShiftAssignment();
        final Shift shift = new Shift();
        final ShiftDate shiftDate = new ShiftDate();
        shiftDate.setDayIndex(dayIndex);
        shift.setShiftDate(shiftDate);
        final ShiftType shiftType = new ShiftType();
        shiftType.setCode(shiftTypeCode);
        shift.setShiftType(shiftType);
        shiftAssignment.setShift(shift);
        shiftAssignment.setEmployee(EmployeeConverter.getEmployee(employee));
        return shiftAssignment;
    }

    public static DayOffRequest getDayOffRequest(Employee employee, String startDate, int dayIndex) {
        final DayOffRequest dayOffRequest = new DayOffRequest();
        final ShiftDate shiftDate = new ShiftDate();
        shiftDate.setDayIndex(dayIndex);
        shiftDate.setDateString(startDate);
        dayOffRequest.setShiftDate(shiftDate);
        dayOffRequest.setEmployee(EmployeeConverter.getEmployee(employee));
        return dayOffRequest;
    }

    public static NurseRoster getNurseRoster(List<Employee> employees, String startDate, int numDays) {
        return getNurseRoster(employees, startDate, numDays, 1);
    }

    public static NurseRoster getNurseRoster(List<Employee> employees, String startDate, int numDays, int coverPerDay) {
        Period period = new Period.Builder()
                .numDays(numDays)
                .startDate(startDate)
                .shiftCoverPerDay(coverPerDay)
                .employees(employees)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .build();
        return new RosterConverter().convert(period);
    }
}
