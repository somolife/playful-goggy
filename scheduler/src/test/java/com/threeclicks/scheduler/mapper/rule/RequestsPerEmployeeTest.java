package com.threeclicks.scheduler.mapper.rule;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.threeclicks.scheduler.api.Employee;
import com.threeclicks.scheduler.api.Employees;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;

import java.util.List;
import java.util.Map;

import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getDayOffRequest;
import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getShiftAssignment;
import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getNurseRoster;
import static org.junit.Assert.assertEquals;

/**
 * @author nigeldev, @date 3/30/18
 */
public class RequestsPerEmployeeTest {
    @Test
    public void myEmployeeGetsTheirRequests() {
        final List<Employee> employees = Employees.createEmployeesFromScratch(2);
        final String startDate = "2018-02-02";
        final String startDateP1 = "2018-02-03";
        final NurseRoster roster = getNurseRoster(employees, startDate, 2);
        final int dayIndex = 0;
        final Employee employee = employees.get(0);
        final Employee employee2 = employees.get(1);

        roster.setDayOffRequestList(Lists.newArrayList(
                getDayOffRequest(employee2, startDate, dayIndex),
                getDayOffRequest(employee, startDate, dayIndex),
                getDayOffRequest(employee, startDateP1, dayIndex + 1)));

        roster.setShiftAssignmentList(Lists.newArrayList(
                getShiftAssignment(employee, dayIndex, "default"),
                getShiftAssignment(employee2, dayIndex + 1, "default")));

        final Map map = new RequestsPerEmployee().apply(roster);
        assertEquals(ImmutableMap.of(1L, Sets.newHashSet(0)), map);
    }
}
