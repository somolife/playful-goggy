package com.threeclicks.scheduler.mapper.rule;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.Employee;
import com.threeclicks.scheduler.api.Employees;
import org.junit.Before;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;

import java.util.List;
import java.util.Map;

import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getNurseRoster;
import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getShiftAssignment;
import static org.junit.Assert.*;

/**
 * @author nigeldev, @date 3/26/18
 */
public class SkillAssignmentPerEmployeeTest {
    private List<Employee> employees = Employees.createEmployeesFromScratch(2);
    private CompositeKey md = CompositeKey.forSkill("MD");
    private CompositeKey np = CompositeKey.forSkill("NP");

    private Employee npEmployee;
    private Employee mdEmployee;

    @Before
    public void setup() {
        this.npEmployee = employees.get(0);
        this.npEmployee.setSkillCode("NP");

        this.mdEmployee = employees.get(1);
        this.mdEmployee.setSkillCode("MD");
    }

    @Test
//     * case2: PT employee should not be assigned to a HOS shift
    public void findWhen_employeeAssignedShiftOutsideSkill() {
        NurseRoster nurseRoster = getNurseRoster(employees, "2018-02-02", 1);
        nurseRoster.setShiftAssignmentList(Lists.newArrayList(
                getShiftAssignment(npEmployee, 0, md.toJson()),
                getShiftAssignment(npEmployee, 1, np.toJson()),
                getShiftAssignment(mdEmployee, 2, md.toJson()),
                getShiftAssignment(mdEmployee, 3, np.toJson()),
                getShiftAssignment(npEmployee, 3, np.toJson())
        ));

        final Map diff = new SkillAssignmentPerEmployee().apply(nurseRoster);
        assertFalse(diff.isEmpty());
        assertEquals(ImmutableMap.of(
                1L, Sets.newHashSet(0),
                2L, Sets.newHashSet(3)), diff);
    }

    @Test
    public void findWhen_employeeAssignedJustRight() {
        NurseRoster nurseRoster = getNurseRoster(employees, "2018-02-02", 1);
        nurseRoster.setShiftAssignmentList(Lists.newArrayList(
                getShiftAssignment(npEmployee, 0, np.toJson()),
                getShiftAssignment(npEmployee, 1, np.toJson()),
                getShiftAssignment(mdEmployee, 0, md.toJson()),
                getShiftAssignment(mdEmployee, 1, md.toJson()),
                getShiftAssignment(npEmployee, 2, np.toJson())
        ));

        final Map diff = new SkillAssignmentPerEmployee().apply(nurseRoster);
        assertTrue(diff.isEmpty());
    }

}
