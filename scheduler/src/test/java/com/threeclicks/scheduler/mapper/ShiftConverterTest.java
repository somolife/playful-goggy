package com.threeclicks.scheduler.mapper;

import com.google.common.collect.ImmutableSet;
import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.ShiftCover;
import com.threeclicks.scheduler.api.cover.CompositeCover;
import com.threeclicks.scheduler.api.cover.DimensionRequirement;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.DayOfWeek;
import org.optaplanner.examples.nurserostering.domain.ShiftDate;
import org.optaplanner.examples.nurserostering.domain.Skill;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author nigeldev, @date 3/18/18
 */
public class ShiftConverterTest {
    private final DayOfWeek dayOfWeek = DayOfWeek.SUNDAY;
    private final String dateString = "2018-02-04";


    @Test
    public void testBaseCase() {
        final String gotoGuy = "jack";
        final int jackCount = 8;
        final String appleGuy = "apple_jack";
        final int appleJackCount = 2;
        final CompositeCover cover = CompositeCover.create(
                new DimensionRequirement(new ShiftCover(jackCount), CompositeKey.forSkill(gotoGuy)),
                new DimensionRequirement(new ShiftCover(appleJackCount), CompositeKey.forSkill(appleGuy))
        );

        ShiftConverter converter = new ShiftConverter(cover,
                Lists.newArrayList(getShiftDate()),
                ImmutableSet.of(getSkill(gotoGuy), getSkill(appleGuy)));

        assertEquals(2, converter.shifts.size());
        assertEquals(ImmutableSet.of("{\"SKILL\":\"jack\"}", "{\"SKILL\":\"apple_jack\"}"),
                converter.shiftTypeMap.keySet());
        assertEquals(2, converter.shiftTypeSkillRequirements.size());
        assertEquals(2, converter.shifts.size());

        assertEquals(appleJackCount, converter.shifts.stream()
                .filter(shift -> shift.getShiftType().getCode().equals("{\"SKILL\":\"apple_jack\"}"))
                .findAny().get().getRequiredEmployeeSize());
        assertEquals(jackCount, converter.shifts.stream()
                .filter(shift -> shift.getShiftType().getCode().equals("{\"SKILL\":\"jack\"}"))
                .findAny().get().getRequiredEmployeeSize());
    }

    private Skill getSkill(String gotoGuy) {
        final Skill skill = new Skill();
        skill.setCode(gotoGuy);
        return skill;
    }

    private ShiftDate getShiftDate() {
        final ShiftDate shiftDate = new ShiftDate();
        shiftDate.setDayOfWeek(dayOfWeek);
        shiftDate.setDateString(dateString);
        shiftDate.setShiftList(Lists.newArrayList());
        return shiftDate;
    }

    @Test
    public void checkWhenASkillHasNoRequirementForADay() {

        final String gotoGuy = "jack";
        final int jackCount = 8;
        final String appleGuy = "apple_jack";
        final int appleJackCount = 2;
        final CompositeCover cover = CompositeCover.create(
                new DimensionRequirement(new ShiftCover(jackCount,
                        Lists.newArrayList(new ShiftCover.DayOfWeekCover(DayOfWeek.SUNDAY.ordinal(), 4)), Lists.emptyList()),
                        CompositeKey.forSkill(gotoGuy)),
                new DimensionRequirement(new ShiftCover(appleJackCount,
                        Lists.newArrayList(new ShiftCover.DayOfWeekCover(DayOfWeek.SUNDAY.ordinal(), 0)), Lists.emptyList()),
                        CompositeKey.forSkill(appleGuy))
        );

        ShiftConverter converter = new ShiftConverter(cover,
                Lists.newArrayList(getShiftDate()),
                ImmutableSet.of(getSkill(gotoGuy), getSkill(appleGuy)));

        assertEquals(1, converter.shifts.size());
        assertEquals(2, converter.shiftTypeSkillRequirements.size());
        assertEquals(1, converter.shifts.size());

        assertFalse(converter.shifts.stream()
                .anyMatch(shift -> shift.getShiftType().getCode().equals("{\"SKILL\":\"apple_jack\"}")));
        assertEquals(4, converter.shifts.stream()
                .filter(shift -> shift.getShiftType().getCode().equals("{\"SKILL\":\"jack\"}"))
                .findAny().get().getRequiredEmployeeSize());
    }
}
