package com.threeclicks.scheduler.mapper.rule;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.threeclicks.scheduler.api.Employee;
import com.threeclicks.scheduler.api.Employees;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;

import java.util.List;
import java.util.Map;

import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getNurseRoster;
import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getShiftAssignment;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author nigeldev, @date 3/27/18
 */
public class ShiftsPerPeriodTest {

    @Test
    public void shiftsPerPeriod_notMet() {
        final List<Employee> employees = Employees.createEmployees(1);
        final int coverPerDay = 3;
        final NurseRoster roster = getNurseRoster(employees, "2018-02-01", 2, coverPerDay);
        roster.setShiftAssignmentList(Lists.newArrayList());

        final Map diff = new ShiftsPerPeriod().apply(roster);
        assertEquals(ImmutableMap.of(0, coverPerDay, 1, coverPerDay), diff);
    }

    @Test
    public void shiftsPerPeriod_wasMet() {
        final List<Employee> employees = Employees.createEmployees(1);
        final NurseRoster roster = getNurseRoster(employees, "2018-02-02", 2);
        final Employee employee = employees.get(0);

        roster.setShiftAssignmentList(Lists.newArrayList(
                getShiftAssignment(employee, 0, "D"),
                getShiftAssignment(employee, 1, "D")
        ));

        assertTrue(new ShiftsPerPeriod().apply(roster).isEmpty());
    }

    @Test
    public void shiftsPerPeriod_wasOff() {
        final List<Employee> employees = Employees.createEmployees(2);
        final NurseRoster roster = getNurseRoster(employees, "2018-02-02", 2, 2);
        final Employee employee = employees.get(0);
        final Employee employee2 = employees.get(1);

        roster.setShiftAssignmentList(Lists.newArrayList(
                getShiftAssignment(employee, 0, "N"),
                getShiftAssignment(employee2, 0, "N"),
                getShiftAssignment(employee, 1, "N")
        ));

        final Map diff = new ShiftsPerPeriod().apply(roster);
        assertFalse(diff.isEmpty());
        assertEquals(ImmutableMap.of(1, 1), diff);
    }

}
