package com.threeclicks.scheduler.mapper;

import com.threeclicks.scheduler.api.Contract;
import com.threeclicks.scheduler.api.Period;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.contract.MinMaxContractLine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author nigeldev, @date 2/26/18
 */
public class ContractConverterTest {
    @Test
    public void testDefaultConversion() {
        Period p = new Period.Builder()
                .numDays(28)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .build();
        ContractConverter converter = new ContractConverter(p);
        org.optaplanner.examples.nurserostering.domain.contract.Contract converted =
                converter.map.values().stream().findFirst().get();
        assertEquals(1, converter.map.size());
        assertEquals(7, converted.getContractLineList().size());

        assertEquals(com.threeclicks.scheduler.api.Contract.DEFAULT_CODE, converted.getCode());
        assertEquals(60, converter.getMaxWeight(converted.getCode()));
        converted.getContractLineList().forEach(contractLine -> {
            switch (contractLine.getContractLineType()) {
                case IDENTICAL_SHIFT_TYPES_DURING_WEEKEND:
                case ALTERNATIVE_SKILL_CATEGORY:
                case SINGLE_ASSIGNMENT_PER_DAY:
                    assertTrue((contractLine).isEnabled());
                    break;
                case TOTAL_ASSIGNMENTS:
                    assertMinMax((MinMaxContractLine) contractLine, 18, 19);
                    break;
                case CONSECUTIVE_FREE_DAYS:
                    assertMinMax((MinMaxContractLine) contractLine, 2, 2);
                    break;
                case CONSECUTIVE_WORKING_DAYS:
                    assertMinMax((MinMaxContractLine) contractLine, 2, 3);
                    break;
                case CONSECUTIVE_WORKING_WEEKENDS:
                    assertMinMax((MinMaxContractLine) contractLine, 1, 2);
                    break;
            }
        });
    }

    private void assertMinMax(MinMaxContractLine line, int min, int max) {
        Assert.assertEquals(min, line.getMinimumValue());
        Assert.assertEquals(max, line.getMaximumValue());
    }

    @Test
    public void testDefaultConversionIsProrated() {
        Period p = new Period.Builder()
                .numDays(7)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .build();
        ContractConverter converter = new ContractConverter(p);
        org.optaplanner.examples.nurserostering.domain.contract.Contract converted =
                converter.map.values().stream().findFirst().get();
        converted.getContractLineList().forEach(contractLine -> {
            switch (contractLine.getContractLineType()) {
                case TOTAL_ASSIGNMENTS:
                    assertMinMax((MinMaxContractLine) contractLine, 4, 5);
                    break;
            }
        });
    }
}
