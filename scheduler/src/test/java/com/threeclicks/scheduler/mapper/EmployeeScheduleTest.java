package com.threeclicks.scheduler.mapper;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.threeclicks.scheduler.api.*;
import com.threeclicks.scheduler.api.cover.CompositeCover;
import com.threeclicks.scheduler.api.cover.DimensionRequirement;
import org.junit.Before;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.Employee;
import org.optaplanner.examples.nurserostering.domain.*;

import java.util.List;
import java.util.Map;

import static com.threeclicks.scheduler.mapper.EmployeeSchedule.*;
import static org.junit.Assert.*;

/**
 * @author nigeldev, @date 2/18/18
 */
public class EmployeeScheduleTest {
    private static final String day1 = "2018-02-02";
    private static final String day2 = "2018-02-03";
    private static final String day3 = "2018-02-04";

    private static CompositeKey nyc = new CompositeKey(ImmutableMap
            .of(CompositeKey.Type.LOCATION, "nyc"));
    private static final String SHIFT_CODE = nyc.toJson();

    @Before
    public void setup() {
        com.threeclicks.scheduler.api.Employee.resetIdGen();
    }

    @Test
    public void testBase() {

        List<com.threeclicks.scheduler.api.Employee> employees = Employees.createEmployees(2);
        int numDays = 5;
        Period basic = new Period.Builder()
                .numDays(numDays)
                .startDate(day1)
                .employees(employees)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .daysOff(1, Lists.newArrayList(day2))
                .daysOn(1, Lists.newArrayList(day3))
                .vacations(0, Lists.newArrayList(day1))
                .build();
        basic.setShiftCover(CompositeCover.create(new DimensionRequirement(new ShiftCover(3), nyc)));
        NurseRoster roster = new RosterConverter().convert(basic);

        List<Employee> employeeList = roster.getEmployeeList();
        roster.setShiftAssignmentList(Lists.newArrayList(
                getShiftAssignment(employeeList.get(0), day2),
                getShiftAssignment(employeeList.get(1), day1),
                getShiftAssignment(employeeList.get(1), day3)
        ));

        EmployeeSchedule schedule = EmployeeSchedule.from(roster, Employees.myById(employees));
        assertNotNull(schedule);
        List<EmployeeSchedule.Header> header = schedule.header;
        assertEquals(11, header.size());
        int i = 0;
        assertEquals(idKey, header.get(i++).key);
        assertEquals(nameKey, header.get(i++).key);
        assertEquals(seniorityKey, header.get(i++).key);
        assertEquals(contractKey, header.get(i++).key);
        assertEquals(skillKey, header.get(i++).key);
        assertEquals(workKey, header.get(i++).key);
        assertEquals(day1, header.get(i++).key);
        assertEquals(day2, header.get(i++).key);
        assertEquals(day3, header.get(i++).key);
        i = 1;
        assertEquals("Name", header.get(i++).name);
        assertEquals("S", header.get(i++).name);
        assertEquals("CTR", header.get(i++).name);
        assertEquals("skill", header.get(i++).name);
        assertEquals("work", header.get(i++).name);
        assertEquals("02/02", header.get(i++).name);
        assertEquals("02/03", header.get(i++).name);
        assertEquals("02/04", header.get(i++).name);

        List<Map<String, Object>> rows = schedule.rows;
        assertEquals(2, rows.size());
        Map<String, Object> first = rows.get(0);
        assertEquals("Empl-1", first.get(nameKey));
        assertEquals("JackOfAll", first.get(skillKey));
        assertEquals(1L, first.get(idKey));
        assertEquals("VA", first.get(day1));
        assertEquals("nyc", first.get(day2));
        assertNull(first.get(day3));

        Map<String, Object> second = rows.get(1);
        assertEquals("RO", second.get(day2));
        assertEquals("nyc", second.get(day3)); //NOTE: this was changed from RW

        assertEquals(2, schedule.weekends.size());
    }

    private ShiftAssignment getShiftAssignment(Employee e, String dateStr) {
        ShiftAssignment assignment = new ShiftAssignment();
        Shift shift = new Shift();
        shift.setShiftDate(getShiftDate(dateStr));
        final ShiftType shiftType = new ShiftType();
        shiftType.setCode(SHIFT_CODE);
        shift.setShiftType(shiftType);
        assignment.setShift(shift);
        assignment.setEmployee(e);
        return assignment;
    }

    private ShiftDate getShiftDate(String dateStr) {
        ShiftDate shiftDate = new ShiftDate();
        shiftDate.setDateString(dateStr);
        return shiftDate;
    }
}
