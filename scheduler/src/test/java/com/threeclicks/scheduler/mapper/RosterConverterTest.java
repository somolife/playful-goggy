package com.threeclicks.scheduler.mapper;

import com.google.common.collect.Lists;
import com.threeclicks.scheduler.api.Contract;
import com.threeclicks.scheduler.api.Employee;
import com.threeclicks.scheduler.api.Employees;
import com.threeclicks.scheduler.api.Period;
import org.junit.Assert;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.domain.NurseRosterParametrization;
import org.optaplanner.examples.nurserostering.domain.ShiftType;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author nigeldev, @date 2/16/18
 */
public class RosterConverterTest {
    @Test
    public void testBasicConversionWithJustOneEmployee() {
        List<Employee> employees = Employees.createEmployees(1);
        Period basic = new Period.Builder()
                .numDays(7)
                .startDate("2018-02-02")
                .shiftCoverPerDay(1)
                .employees(employees)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .build();
        NurseRoster roster = new RosterConverter().convert(basic);
        Assert.assertNotNull(roster);

        assertEquals(1, roster.getEmployeeList().size());
        assertEquals(7, roster.getShiftDateList().size());

        assertEquals(roster.getContractList().get(0), roster.getEmployeeList().get(0).getContract());
    }

    @Test
    public void testConversionWithBasicPeriod() {
        List<Employee> employees = Employees.createEmployees(5);
        Period basic = new Period.Builder()
                .numDays(7)
                .startDate("2018-02-02")
                .shiftCoverPerDay(4)
                .employees(employees)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .daysOff(0, Lists.newArrayList("2018-02-04"))
                .daysOff(1, Lists.newArrayList("2018-02-02", "2018-02-05", "2018-02-06"))
                .daysOff(2, Lists.newArrayList("2018-02-02", "2018-02-03"))
                .daysOff(4, Lists.newArrayList("2018-02-05", "2018-02-07"))
                .daysOn(0, Lists.newArrayList("2018-02-05", "2018-02-06"))
                .build();
        NurseRoster roster = new RosterConverter().convert(basic);
        Assert.assertNotNull(roster);

        assertEquals(5, roster.getEmployeeList().size());
        assertEquals(7, roster.getShiftDateList().size());
        Assert.assertNotNull(roster.getNurseRosterParametrization());
        assertEquals(1, roster.getShiftTypeList().size());
        assertEquals("{\"SKILL\":\"JackOfAll\"}", roster.getShiftTypeList().get(0).getCode());

        assertEquals(7, roster.getShiftList().size());
        assertEquals(8, roster.getDayOffRequestList().size());
        assertEquals(2, roster.getDayOnRequestList().size());

        assertEquals(28, roster.getShiftAssignmentList().size());

        assertEquals(1, roster.getEmployeeList().get(0).getDayOffRequestMap().size());
        assertEquals(3, roster.getEmployeeList().get(1).getDayOffRequestMap().size());

        assertEquals(1, roster.getSkillList().size());
        assertEquals("JackOfAll",roster.getSkillList().get(0).getCode());

        assertEquals(5, roster.getSkillProficiencyList().size());

        NurseRosterParametrization parametrization = roster.getNurseRosterParametrization();
        assertEquals("2018-02-02", parametrization.getPlanningWindowStart().getDateString());
        assertEquals("2018-02-02", parametrization.getFirstShiftDate().getDateString());
        assertEquals("2018-02-08", parametrization.getLastShiftDate().getDateString());

        assertEquals("{\"SKILL\":\"JackOfAll\"} (Day) of Fr 02/02", roster.getShiftList().get(0).getLabel());
        assertEquals("Fr 02/02", roster.getShiftDateList().get(0).getLabel());
        assertEquals("Sa", roster.getShiftDateList().get(1).getDayOfWeek().getLabel());
        assertEquals("Su", roster.getShiftDateList().get(2).getDayOfWeek().getLabel());

        assertNull(roster.getShiftAssignmentList().get(0).getContract());

        ShiftType shiftType = roster.getShiftTypeList().get(0);
        assertEquals("08:00:00", shiftType.getStartTimeString());
        assertEquals("20:30:00", shiftType.getEndTimeString());
        assertFalse(shiftType.isNight());
    }
}
