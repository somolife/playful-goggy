package com.threeclicks.scheduler.mapper.rule;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.Employee;
import com.threeclicks.scheduler.api.Employees;
import org.junit.Test;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;

import java.util.List;
import java.util.Map;

import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getNurseRoster;
import static com.threeclicks.scheduler.mapper.utils.NurseRosterUtils.getShiftAssignment;
import static org.junit.Assert.assertEquals;

/**
 * @author nigeldev, @date 3/26/18
 */
public class PatternsPerEmployeeTest {

    @Test
    // basic case: any MD scheduled to work the HOS shift, must get the next day off
    public void testDayAfterHospitalAssignmentIsUnassigned() {
        List<Employee> employees = Employees.createEmployeesFromScratch(2);
        CompositeKey hos = new CompositeKey(ImmutableMap.of(CompositeKey.Type.LOCATION, "hos"));
        CompositeKey nyc = new CompositeKey(ImmutableMap.of(CompositeKey.Type.LOCATION, "nyc"));

        final Employee employee = employees.get(0);
        final int numDays = 3;

        NurseRoster nurseRoster = getNurseRoster(employees, "2018-02-02", numDays);
        nurseRoster.setShiftAssignmentList(Lists.newArrayList(
                getShiftAssignment(employee, 0, nyc.toJson()),
                getShiftAssignment(employees.get(1), 0, hos.toJson()),
                getShiftAssignment(employee, 1, hos.toJson()),
                getShiftAssignment(employee, 2, hos.toJson())
        ));

        final Map m = new PatternsPerEmployee(hos).apply(nurseRoster);
        final Map m1 = (Map) m.get("coverDimensionFollowedByFreeDay");
        final Map m2 = (Map) m1.get(hos.toJson());
        assertEquals(ImmutableMap.of(1L, Sets.newHashSet(2)), m2);
    }
}
