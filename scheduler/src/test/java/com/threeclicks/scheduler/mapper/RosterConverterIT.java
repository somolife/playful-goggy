package com.threeclicks.scheduler.mapper;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.threeclicks.scheduler.api.*;
import com.threeclicks.scheduler.api.cover.CompositeCover;
import com.threeclicks.scheduler.api.CompositeKey;
import com.threeclicks.scheduler.api.cover.DimensionRequirement;
import org.junit.Assert;
import org.junit.Test;
import org.optaplanner.core.api.domain.solution.Solution;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.config.solver.EnvironmentMode;
import org.optaplanner.examples.nurserostering.domain.NurseRoster;
import org.optaplanner.examples.nurserostering.domain.ShiftAssignment;
import org.optaplanner.examples.nurserostering.solver.SolverBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.threeclicks.scheduler.api.CompositeKey.Type.LOCATION;
import static com.threeclicks.scheduler.api.CompositeKey.Type.SKILL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author nigeldev, @date 2/16/18
 */
public class RosterConverterIT {
    @Test
    public void testSolverWithNoRequests() throws IOException {
        List<Employee> employees = Employees.createEmployees(4);
        Period basic = new Period.Builder()
                .numDays(14)
                .startDate("2018-02-02")
                .shiftCoverPerDay(3)
                .employees(employees)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .build();
        NurseRoster roster = new RosterConverter().convert(basic);
        final Solver<Solution> solver = getSolver();
        NurseRoster solution = (NurseRoster) solver.solve(roster);
        checkShiftsAssigned(solution);
        checkDayOffAssignments(solution, 0);
        checkEmployeeAssignment(solution, 4, 9);
    }

    private Solver<Solution> getSolver() {
        return new SolverBuilder().envMode(EnvironmentMode.REPRODUCIBLE).timeLimit(1).build();
    }

    @Test
    public void testSolverWithSomeSkills() throws IOException {
        List<Employee> employees = Employees.createEmployees(5);
        employees.forEach(employee -> employee.setSkillCode("nurse"));
        final Employee first = employees.get(0);
        first.setSkillCode("manager");
        final int defaultCover = 3;
        final int numDays = 3;
        Period basic = new Period.Builder()
                .numDays(numDays)
                .startDate("2018-02-02")
                .shiftCoverPerDay(defaultCover)
                .employees(employees)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .build();
        final DimensionRequirement r1 = new DimensionRequirement(
                new ShiftCover(1),
                new CompositeKey(ImmutableMap.of(
                        SKILL, "manager",
                        LOCATION, "nyc")
                ));
        final DimensionRequirement r2 = new DimensionRequirement(
                new ShiftCover(1),
                new CompositeKey(ImmutableMap.of(
                        SKILL, "nurse",
                        LOCATION, "nyc")
                ));
        final CompositeKey hoboken = new CompositeKey(ImmutableMap.of(
                SKILL, "nurse",
                LOCATION, "hoboken")
        );
        final DimensionRequirement r3 = new DimensionRequirement(
                new ShiftCover(1),
                hoboken);
        basic.setShiftCover(CompositeCover.create(r1, r2, r3));
        NurseRoster roster = new RosterConverter().convert(basic);
        assertEquals(2, roster.getSkillList().size());
        assertEquals(3, roster.getShiftTypeList().size());
        assertEquals(3, roster.getShiftTypeSkillRequirementList().size());
        assertTrue(roster.getShiftTypeSkillRequirementList().stream()
                .map(req -> req.getShiftType().getCode())
                .anyMatch(hoboken.toJson()::equals));
        assertEquals(5, roster.getSkillProficiencyList().size());

        NurseRoster solution = (NurseRoster) getSolver().solve(roster);
        checkShiftsAssigned(solution);

        assertEquals(3, solution.getShiftAssignmentList().stream()
                .filter(assignment -> assignment.getEmployee().getId().equals(first.getId())).count());
    }

    @Test
    public void testSolverWithMultipleDayOffs() throws IOException {
        List<Employee> employees = Employees.createEmployees(5);
        Period basic = new Period.Builder()
                .numDays(14)
                .startDate("2018-02-02")
                .shiftCoverPerDay(3)
                .employees(employees)
                .contracts(Lists.newArrayList(Contract.defaultContract()))
                .daysOff(0, Lists.newArrayList("2018-02-04"))
                .daysOff(1, Lists.newArrayList("2018-02-05", "2018-02-06"))
                .daysOff(2, Lists.newArrayList("2018-02-02", "2018-02-03"))
                .daysOff(3, Lists.newArrayList("2018-02-08"))
                .daysOff(4, Lists.newArrayList("2018-02-06", "2018-02-07"))
                .build();
        NurseRoster roster = new RosterConverter().convert(basic);

        NurseRoster solution = (NurseRoster) getSolver().solve(roster);
        checkShiftsAssigned(solution);
        checkDayOffAssignments(solution, 4);
        checkEmployeeAssignment(solution, 5, 6);
    }

    private void checkDayOffAssignments(NurseRoster solution, int atMost) {
        List<ShiftAssignment> assignedOnDayOff = solution.getShiftAssignmentList().stream()
                .filter(shiftAssignment -> {
                    String dateString = shiftAssignment.getShiftDate().getDateString();
                    return shiftAssignment.getEmployee().getDayOffRequestMap().keySet().stream()
                            .anyMatch(shiftDate -> shiftDate.getDateString().equals(dateString));
                }).collect(Collectors.toList());
        assertTrue(assignedOnDayOff.size() <= atMost);
    }

    private void checkEmployeeAssignment(NurseRoster solution, int numAssigned, int atLeast) {
        Map<Long, Long> empMap = solution.getShiftAssignmentList().stream()
                .collect(Collectors.groupingBy(o -> o.getEmployee().getId(), Collectors.counting()));
        assertEquals(numAssigned, empMap.size());
        assertTrue(empMap.entrySet().stream().map(Map.Entry::getValue).allMatch(aLong -> aLong >= atLeast));
    }

    private void checkShiftsAssigned(NurseRoster solution) {
        Assert.assertNotNull(solution);
        assertTrue(solution.getShiftAssignmentList().stream()
                .allMatch(shiftAssignment -> shiftAssignment.getEmployee() != null));
    }
}
